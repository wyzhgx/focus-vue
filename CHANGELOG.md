# 版本变更日志

## 1.0.0

由开发测试版的`focus-test-vue`迁移至正式版本插件发布

## 1.0.1

变更 `package.json` 文件、`README`文件信息

## 1.0.2

变更 `package.json` 文件 `repository` 信息

## 1.0.3

支持自定义焦点属性

## 1.0.4

支持自定义焦点属性 2

## 1.0.5

增加默认焦点配置以及同一标记焦点组默认选中配置

```javascript
// 初始化默认焦点配置
<div v-items="data" data-appoints>
// 同一标记焦点列表默认选中配置
<div v-items="data" data-sign="nav" data-assign>
<div v-items="data" data-sign="nav">
```

## 1.0.6

增加页面统一返回逻辑配置

```javascript
Vue.use(focus, {
  back: function () {
    // 默认返回方法
  }
})
```

## 1.0.9

处理通过`openPop`打开弹窗时默认焦点初始化冲突的问题

## 1.0.10

处理当自定义指令`v-items`绑定在组件上时，获取相关绑定事件失败的问题,以及增加压缩版`cdn`引入文件

## 1.0.11

规范`cdn`引入文件命名

## 1.0.13

1. 改进焦点初始化方法`initFocus`和`checkPosition`，数据变更后可以直接调用而不用在`$nextTick`中调用。
2. 调整`initFocus`方法接收参数为函数，函数返回为`Boolean`值，来指定初始化焦点位置

```javascript
export default {
  mounted() {
    this.$Focus.initFocus((value) => value.data.id === 2)
    // this.$Focus.checkPosition(0, 0);
  }
}
```

## 1.0.14

1. 处理默认返回方法：将`history.back`直接赋值给变量执行报错<font color="red">Uncaught TypeError: Illegal invocation</font>的问题
2. `group`计算容错改进。
3. 焦点初始化方法`initFocus`和`checkPosition`增加回调，用于焦点初始化执行完成后的定制逻辑

## 1.0.15

1. 增加同一标记选中类名的配置
2. 焦点选中状态添加选中类名
3. 简化初始化焦点方法的调用；保留`initFocus`，可以接收 3 个参数规则参考[开发文档](https://wyzhgx.gitee.io/epg-focus/config/api.html#initfocus)

## 1.0.17

1. 变更入口文件；新入口文件为编译后的文件。
2. 改进 `dataset` 兼容性

## 1.0.18

调整配置文件环境变量

## 1.0.19

处理`1.0.18`引入报错问题

## 1.1.1

1. 调用`checkSign`方法时，更新对应元素节点选中类名；
2. 为 `prev、next、end、start` 增加执行目标焦点的返回；
3. 添加含有同一标记`sign`的节点之间选中状态的移动变更方法`prevSign`、`nextSign`；
4. 增加丰富`end`、`start`方法功能，使其支持标记选中状态的移动；
5. 优化`checkPosition`方法，如果检测到焦点含有`sign`标记，则将焦点移动到选中标记上（如果没有标记选中状态，则根据通用焦点移动规则移动并添加选中状态）；
6. 优化`createFocus`方法，如果检测到焦点含有`sign`标记，则变更同类标记选中状态以及对应元素上的类名。

## 1.2.0

1. 支持 `SSR` 引入
2. 初始化焦点时，按标记检测焦点处理逻辑改进
3. 节点获焦和失焦的响应放到添加移除焦点之后

## 1.2.1

1. 增加`prev`、`next`焦点移动方法触发换行时，可以执行回调
2. 优化未注册焦点时，初始化焦点报标记错误
3. 调整 package.json 插件入口文件配置；babel 编译配置变更

## 1.2.2

1. package.json 增加`module`配置
2. 改进`start`、`end`执行逻辑、效果类似于执行`left`或`right`到边界位置，增加对指定`group`区域内移动的支持
3. 改进`prev`、`next`换行执行逻辑；如果换行时焦点含有`group`标记，则优先在指定`group`区域内换行移动焦点;如果不含`group`标记则执行正常换行逻辑
4. 焦点元素解绑时，同步对应的默认选中状态

## 1.2.3

1. 优化切换焦点时同一标记的选中状态处理逻辑

## 2.0.0/2.0.1

1. 重构焦点移动计算逻辑
2. 增加对嵌套`group`布局的支持
3. 对[嵌套路由](https://router.vuejs.org/zh/guide/essentials/nested-routes.html)的应用支持更友好,暂不支持对[命名视图](https://router.vuejs.org/zh/guide/essentials/named-views.html)的兼容
4. 增加路由[keepAlive 相关配置](https://wyzhgx.gitee.io/epg-focus/v2/#includeattr)，使用规范参考[keepAlive 应用方案](https://wyzhgx.gitee.io/epg-focus/v2/keep-alive.html)；
5. 失焦事件`vLostFocus`变更为`vBlur`
6. $Focus 实例对象上移除`items`、`groups`两个列表的获取
7. 实例对象增加`getSignList`方法获取指定分组标记列表
8. 增加[事件配置](https://wyzhgx.gitee.io/epg-focus/v2/#keymaps)

## 2.0.2

清除日志打印

## 2.0.3

1. 优化`group`注册移除逻辑
2. 优化列表计算逻辑
3. 优化`routeview`部分加载延迟导致的焦点计算错误问题

## 2.0.4-beta.0

1. 增加 `pools`、`views` 相关解绑逻辑
2. 优化处理 `keepalive` 含有最大模版数量限定时相关注册解绑逻辑

## 2.0.4-beta.1、2.0.4

1. `pools`、`views` 解绑逻辑改进

## 2.0.5-beta.0

1. 增加对`group`中列表为空时焦点移动的兼容处理
2. 优化节点销毁时，在含有嵌套路由的页面节点销毁异常问题

## 2.0.5-beta.1、2.0.5

1. 优化移入`group`中的计算逻辑

## 2.0.6

1. 优化局部更新涉及到默认焦点`data-appoints`时的焦点处理逻辑

## 2.0.7-beta.0

1. 修改默认容错距离`failoverDist`为 0
2. 优化根据点位置初始化焦点逻辑
3. 增加`group`节点绑定`sign`属性判断提示

## 2.0.7-beta.1

1. 优化按键锁定逻辑
2. 优化焦点组内焦点计算逻辑

## 2.0.7-beta.2

1. 优化获取焦点计算逻辑
2. 调整`prev`、`next`方法参数含义及计算逻辑；接收的第一个参数为是否限制在区域内移动

## 2.0.8-beta.0

1. 新增`longTap`长按事件
2. 增补弹窗交互处理逻辑，增加方法`addPopEvent`，打开弹窗时激活对应弹窗的事件

## 2.0.8-beta.1

1. 重构事件交互逻辑，增加按键长按事件
2. 优化打开关闭弹窗逻辑，支持逐层开启关闭弹层，以及全关闭弹层
3. 移除按键间隔时长设置
4. 增加无交互持续时间及相关事件配置

## 2.0.8-beta.2

1. 无交互时间配置增加单位识别（h\m\s）
2. 优化`closePop`关闭弹窗逻辑，支持根据弹窗打开顺序依次关闭弹窗
3. 优化`openPop`打开弹窗逻辑，传空字符串时，则直接回到默认层级，传指定弹窗标记时为打开对应弹窗

## 2.0.8-beta.3

1. 焦点计算性能优化，添加观察者模式`intersectionObserver`
2. `initFocus`方法增加支持直接传入`pointer`对象赋予焦点状态
3. 增加`rootMargin`配置项，用来限制焦点计算范围
4. 长按响应时间调整为`650ms`
5. 缓存焦点位置信息

## 2.0.8-beta.5

1. 因兼容问题，移除观察者模式`intersectionObserver`，并移除`rootMargin`参数配置
2. 增加`useKeyUp`配置参数，指定是否使用`keyup`事件来响应按键操作，默认为`false`；该参数配置为`true`时支持长按操作
3. 保留`limitTime`配置参数，仅在`useKeyUp`参数配置为`false`时生效；用于限制`keydown`事件响应间隔，默认为 200ms
4. 引入`getBoundingClientRect`方法获取元素位置信息，减少焦点移动时计算量

## 2.0.8

1. 正式版发布

## 2.0.9

1. 更新数据类型判断逻辑
2. 事件回调中兼容全局`event`事件
3. 改进返回事件处理逻辑
4. 将`inactiveTime`边界设置为 5 分钟（包含 5 分钟）
5. 按键响应开关`monitor`默认为`true`，允许不引入路由的页面可以使用按键逻辑

## 2.0.10-beta.0

1. 调整获焦/选中状态类名添加移除方式
2. 弹窗标记列表获取逻辑改进

## 2.0.10-beta.1

1. 调整获焦/选中状态类名添加移除方式
2. 调整长按事件响应逻辑

## 2.0.10-beta.2

1. 调整弹窗标记列表获取逻辑

## 2.0.10

1. 调整分组错误提示日志
2. 正式版本发布

## 2.0.11

1. 处理执行`prevSign`选中状态显示异常问题

## 3.0.0

1. 使用 ts 改写
2. 支持 vue3
3. 路由命名区域计算逻辑异常问题调整
4. 支持鼠标点击和悬停响应

## 3.0.1

1. 调整确认键响应锁定逻辑
