import typescript from '@rollup/plugin-typescript';
import resolve from '@rollup/plugin-node-resolve';
import commonjs from '@rollup/plugin-commonjs';
import json from '@rollup/plugin-json';
import terser from '@rollup/plugin-terser';
export default {
  input: 'src/index.ts', // 入口文件
  output: [
    {
      file: 'dist/epg-focus-vue.cjs.js', // CommonJS 输出
      format: 'cjs',
      sourcemap: true,
    },
    {
      file: 'dist/epg-focus-vue.esm.js', // ESM 输出
      format: 'esm',
      sourcemap: true,
    },
    {
      file: 'dist/epg-focus-vue.min.js', // 压缩后的文件
      format: 'iife',
      name: 'MyFocus',
      plugins: [terser()], // 单独针对该文件启用压缩
      sourcemap: true,
    },
  ],
  plugins: [
    resolve(), // 处理模块路径
    typescript({
      tsconfig: './tsconfig.json',
      tslib: require('tslib'),    // 显式传递 tslib 模块
    }), // TypeScript 支持
    commonjs(), // 转换 CommonJS 模块为 ES6
    json(), // 支持导入 JSON 文件
    // terser(), // 全局启用压缩
  ],
  external: ['vue', 'vue-router', 'pinia'], // 将某些模块标记为外部依赖，不打包到最终文件
};
