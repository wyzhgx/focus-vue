import { RouteLocationNormalized } from "vue-router";
import Views from "../types/views";
import Calc from "./calc";
import { DirectionStr, OptionType } from "../types/focus";
import Creater from "../types/creater";
export default class MoveRule extends Calc {
    private _views;
    private _path;
    private selectName;
    private focusName;
    private popSignList;
    protected constructor(options?: OptionType);
    get pointer(): Creater | undefined;
    get popSign(): string;
    /**
     * 获取焦点所属Views
     * @param routeDepath 节点路由深度
     * @param routeVpath 节点路由路径
     * @returns
     */
    protected getView(routeDepath: number, routeVpath: string): Views;
    /**
     * 根据路由创建视图层，并变更当前路径对象
     * @param {RouteLocationNormalized} to VUE路由守卫中的目标
     */
    get createView(): (to: RouteLocationNormalized) => false | undefined;
    /**
     * 获取指定popup弹层基础焦点列表
     * @param createrOrPopup 焦点对象或弹层标记
     * @returns Creater[]
     */
    private getList;
    /**
     * 按当前路径遍历Views对象
     * @param func 视图匹配方法
     * @param views 基础视图
     * @returns
     */
    private ergodicViews;
    /**
     * 打开新的弹层
     * @param popup 弹层标记
     * @param callBack 切换弹层后的会调，可能未获焦
     */
    get openPop(): (popup?: string, callBack?: () => void) => void;
    /**
     * 关闭弹窗
     */
    get closePop(): () => void;
    /**
     * 设置焦点
     * @param next 焦点对象
     * @returns Creater | undefined
     */
    get checkFocus(): (next: Creater | undefined) => Creater | undefined;
    /**
     * 焦点移动方法
     * @param direction 移动方向（left,right,up,down）
     * @param pointer 焦点对象
     * @returns Creater | undefined
     */
    get moveFocus(): (direction: DirectionStr, pointer?: Creater | undefined) => Creater | undefined;
    /**
     * 根据位置信息设置焦点
     * @param left 元素X方向偏移量
     * @param top 元素Y方向偏移量
     * @param callBack 设置焦点后需要执行的方法(非必传)
     * @returns Creater | undefined
     */
    get checkPosition(): (left: number, top: number, callBack?: (prop?: Creater) => void) => Creater | undefined;
    /**
     * 自定义条件初始化焦点
     * @param check 焦点筛选条件方法，并返回Boolean值
     * @param callBack 非必传字段、初始化焦点后需要执行的方法
     */
    get createFocus(): (check: (prop: Creater) => boolean, callBack?: (prop?: Creater) => void) => void;
    /**
     * 通用焦点初始化方法
     * @param a 焦点X方向偏移量/焦点筛选条件方法
     * @param b 焦点Y方向偏移量/焦点初始化后的回调
     * @param c 焦点初始化后的回调
     */
    get initFocus(): (a: number | ((prop: Creater) => boolean) | Creater, b?: number | ((prop?: Creater) => void), c?: (prop?: Creater) => void) => void;
    /**
     * 下一个焦点，仅处理当前弹层水平方向的下一个
     * @param flag 是否在限定区域内移动
     * @param callBack 触及边界且换行成功后执行的方法
     * @returns Creater | undefined
     */
    get next(): (flag: boolean, callBack: (prop?: Creater) => void) => Creater | undefined;
    /**
     * 上一个焦点，仅处理当前弹层水平方向的上一个
     * @param flag 是否在限定区域内移动
     * @param callBack 触及边界且换行成功后执行的方法
     * @returns Creater | undefined
     */
    get prev(): (flag: boolean, callBack: () => void) => Creater | undefined;
    /**
     * 移动到当前焦点所在行的第一个|移动标记到焦点所在行的第一个
     * @param createrOrFlag 是否限定区域|含有sign标记的焦点对象
     * @returns Creater | undefined
     */
    get start(): (createrOrFlag: Creater | boolean) => Creater | undefined;
    /**
     * 移动到当前焦点所在行的最后一个|移动标记到焦点所在行的最后一个
     * @param createrOrFlag 是否限定区域|含有sign标记的焦点对象
     * @returns Creater | undefined
     */
    get end(): (createrOrFlag: boolean | Creater) => Creater | undefined;
    /**
     * 清除当前层级焦点内容
     */
    get clearFocus(): () => void;
    /**
     * 处理同一标记类型，当前选中状态
     * @param sign 元素标记名称
     * @param fn 返回布尔类型来标记当前焦点选中或未选中
     * @param popup 节点所在层级标记（非必传）
     */
    get checkSign(): (sign: string, fn: (prop: Creater) => boolean, popup?: string) => void;
    /**
     * 根据弹层和标签标记名称获取标记列表
     * @param createrOrSign 分组标记名称
     * @param popup 弹层标记名称
     * @returns Creater[]
     */
    get getSignList(): (createrOrSign: string | Creater, popup?: string) => Creater[];
    /**
     * 获取指定标记类型选中状态的数据内容
     * @param createrOrSign 元素标记名称|焦点元素
     * @param popup 节点所在层级标记（非必传）
     * @returns Creater | undefined
     */
    get getSign(): (createrOrSign: string | Creater, popup?: string) => Creater | undefined;
    /**
     * 下一个标记移动方法
     * @param sign 标记名称
     * @param direction 方向（默认为向右移动）
     * @param popup 弹窗标记，非必传
     * @returns Creater | undefined
     */
    get nextSign(): (sign: string, direction?: DirectionStr, popup?: string) => Creater | undefined;
    /**
     * 上一个标记移动方法
     * @param sign 标记名称
     * @param direction 方向（默认为向左移动）
     * @param popup 弹窗标记，非必传
     * @returns Creater | undefined
     */
    get prevSign(): (sign: string, direction?: DirectionStr, popup?: string) => Creater | undefined;
}
