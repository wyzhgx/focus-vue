import { FocusRouteObj } from '../types/focus';
/**
 * 设置元素属性
 * @param el 元素节点
 * @param name 名称
 * @param value 值
 */
export declare const setDataSet: (el: HTMLElement, name: string, value?: string) => void;
/**
 * 获取节点路由区域信息
 * @param elem 元素节点
 * @returns 焦点路由相关信息
 */
export declare const checkRouteViewDepath: (elem: HTMLElement) => FocusRouteObj;
/**
 * 计算元素节点位置信息
 * @param el DOM节点
 * @param key 方向
 * @returns number
 */
export declare const getOffsetParent: (el: HTMLElement, key: "top" | "left") => number;
/**
 * 将undefined、null等转化为""
 * @param str 字符串
 * @returns string
 */
export declare const parseStr: (str: any) => string;
/**
 * 时间单位转换，单位毫秒
 * @param time 时间
 * @returns number
 */
export declare const convertUnits: (time: string | number) => number;
/**
 * 获取弹窗标记
 * @param popup 弹窗标记
 * @returns 弹窗名称
 */
export declare const getPopupStr: (popup?: string) => string;
declare const _default: {
    getPopupStr: (popup?: string) => string;
    setDataSet: (el: HTMLElement, name: string, value?: string) => void;
    checkRouteViewDepath: (elem: HTMLElement) => FocusRouteObj;
    getOffsetParent: (el: HTMLElement, key: "top" | "left") => number;
    parseStr: (str: any) => string;
};
export default _default;
