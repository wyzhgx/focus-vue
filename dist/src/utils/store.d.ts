declare const _default: import("pinia").StoreDefinition<"focusKeepAlive", Pick<{
    includeStr: import("vue").Ref<string, string>;
    setPages: (pageNames: string) => void;
    getPages: () => string;
    delPage: (pageName: string) => void;
}, "includeStr">, Pick<{
    includeStr: import("vue").Ref<string, string>;
    setPages: (pageNames: string) => void;
    getPages: () => string;
    delPage: (pageName: string) => void;
}, never>, Pick<{
    includeStr: import("vue").Ref<string, string>;
    setPages: (pageNames: string) => void;
    getPages: () => string;
    delPage: (pageName: string) => void;
}, "setPages" | "getPages" | "delPage">>;
export default _default;
