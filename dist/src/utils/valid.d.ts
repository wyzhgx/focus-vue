/**
 * 判断是否为对象类型
 * @param obj 任意数据
 * @returns boolean
 */
export declare const isObject: (obj: any) => boolean;
/**
 * 判断是否为字符串类型
 * @param str 任意数据
 * @returns boolean
 */
export declare const isString: (str: any) => boolean;
/**
 * 判断是否为数组类型
 * @param arr 任意数据
 * @returns boolean
 */
export declare const isArray: (arr: any) => boolean;
/**
 * 判断是否为函数类型
 * @param fun 任意数据
 * @returns boolean
 */
export declare const isFunction: (fun: any) => boolean;
/**
 * 判断是否为数字类型
 * @param num 任意数据
 * @returns boolean
 */
export declare const isNumber: (num: any) => boolean;
/**
 * 判断是否为布尔类型
 * @param flag 任意数据
 * @returns boolean
 */
export declare const isBoolean: (flag: any) => boolean;
/**
 * 判断是否为undefined
 * @param undef 任意数据
 * @returns boolean
 */
export declare const isUndefined: (undef: any) => boolean;
/**
 * 判断是否为null
 * @param nul 任意数据
 * @returns boolean
 */
export declare const isNull: (nul: any) => boolean;
/**
 * 判断对象是否含有对应的字段
 * @param obj 对象
 * @param prop 字段名称
 * @returns boolean
 */
export declare const hasOwn: (obj: object, prop: string) => boolean;
