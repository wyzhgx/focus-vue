import { App, DirectiveBinding } from 'vue';
import Creater from '../types/creater';
import { FocusEventName, OptionType } from '../types/focus';
import MoveRule from './moveRule';
export default class Service extends MoveRule {
    private _monitor;
    private _vm;
    private activeListenFlag;
    private _durationState;
    private _durationTimeout;
    private _baseEvents;
    private popEvents;
    private _version;
    protected defOption: OptionType;
    constructor(app: App, options?: OptionType);
    get monitor(): boolean;
    set monitor(flag: boolean);
    /**
     * 查看版本号
     */
    get version(): string;
    /**
     * 重置按键响应事件
     */
    get resetEvent(): () => void;
    /**
     * 添加事件
     * @param eventObj 事件对象
     * @param popup 弹层
     */
    get addEvent(): (eventObj: { [k in FocusEventName]?: () => void; }, popup?: string) => void;
    private getRouteName;
    private startEventListen;
    private callEvent;
    /**
     * 判断并启动非活跃状态监听
     */
    private startActiveListen;
    /**
     * 退出非活跃状态
     */
    private exitInactive;
    /**
     * 非活跃状态计时
     */
    private timeoutInactive;
    /**
     * 检测当前视图是否有未卸载的焦点/视图对象
     * @param views 当前节点所在视图对象
     * @returns 所有子视图、焦点是否已卸载
     */
    private hasChildren;
    /**
     * 注册节点
     * @param {Creater} creater 节点对象
     */
    get register(): (creater: Creater) => void;
    /**
     * 更新焦点/区域
     * @param elem 元素
     * @param binding 绑定信息
     */
    get updater(): (elem: HTMLElement, binding: DirectiveBinding) => void;
    /**
     * 卸载焦点及对应view
     * @param elem 元素
     */
    get remover(): (elem: HTMLElement) => void;
}
