import Creater from '../types/creater';
import { DirectionStr, FocusEventName, FocusKeyMaysKeys, OptionType } from '../types/focus';
import Views from '../types/views';
/**
 * 焦点算法
 */
export default class Calc {
    protected blockEnter: boolean;
    private failoverDist;
    private _useKeyUp;
    private _direction;
    private keyMaps;
    protected constructor(options?: OptionType);
    get useKeyUp(): boolean;
    get direction(): string;
    get enterKeyCode(): number;
    /**
     * 计算焦点元素到某点的最短距离
     * @param left 焦点left
     * @param top 焦点top
     * @param width 焦点width
     * @param height 焦点height
     * @param x 基准点x
     * @param y 基准点y
     * @returns number
     */
    protected rulesSurfaceToPoint(left: number, top: number, width: number, height: number, x: number, y: number): number;
    /**
     * 判断元素某一方向上是否交错
     * 参数取两个元素同一方向上的起点及相应尺寸
     * @param pb prev元素起点
     * @param pc prev元素相应方向的尺寸
     * @param nb next元素起点
     * @param nc next元素相应方向的尺寸
     * @returns number
     */
    protected getOverlay(pb: number, pc: number, nb: number, nc: number): number;
    /**
     * 对比返回距离x,y坐标更近的焦点
     * @param x 基准点x
     * @param y 基准点y
     * @param target 下一个焦点
     * @param cache 缓存焦点对象
     * @returns Creater | undefined
     */
    protected rulesPoint(x: number, y: number, target: Creater, cache?: Creater): Creater | undefined;
    /**
     * 通用元素焦点移动计算规则
     * @param point 焦点元素对象
     * @param target 待做对比的焦点元素对象
     * @param direction 焦点移动方向
     * @param cache 满足条件的最优的焦点元素对象
     * @returns Creater | undefined
     */
    protected rules(point: Creater, target: Creater, direction: DirectionStr, cache?: Creater): Creater | undefined;
    /**
     * 移入Views或group计算规则
     * @param pointer 区域限制对象
     * @param target 待做对比的焦点对象
     * @param direction 焦点移动方向
     * @param cache 满足条件的最优的焦点元素对象
     * @returns Creater | undefined
     */
    protected rulesDist(pointer: Creater, target: Creater, direction?: DirectionStr, cache?: Creater): Creater | undefined;
    /**
     * 移入新group的焦点计算方法
     * @param direction 移动方向
     * @param pointer 焦点元素对象
     * @returns Creater | undefined
     */
    protected rulesInToGroup(direction: DirectionStr, pointer: Creater): Creater | undefined;
    /**
     * 移入新Views的焦点计算方法
     * @param direction 方向
     * @param views 对应Views对象
     * @param popSign 弹层标记
     * @returns Creater | undefined
     */
    protected rulesInToViews(direction: DirectionStr, views: Views, popSign: string): Creater | undefined;
    /**
     * 查找相邻节点规则,如果含有group标记，则仅在对应group内查找相邻节点
     * @param direction 移动方向
     * @param pointer 焦点元素对象
     * @param inGroup 是否限定在路由区域内移动
     * @param popSign 弹层标记
     * @returns Creater | undefined
     */
    protected rulesInArea(direction: DirectionStr, pointer: Creater, inGroup?: boolean, popSign?: string): Creater | undefined;
    /**
     * 查找指定方向上与pointer含有相同标记的下一个焦点
     * @param direction 方向
     * @param pointer 焦点对象
     * @returns Creater | undefined
     */
    protected rulesInSign(direction: DirectionStr, pointer: Creater): Creater | undefined;
    /**
     * 换到下一行第一个
     * @param pointer 焦点对象
     * @param popSign 弹层标记
     * @param inGroup 是否限定区域
     * @returns Creater | undefined
     */
    protected rulesTurnDown(pointer: Creater, popSign: string, inGroup?: boolean): Creater | undefined;
    /**
     * 换到上一行最后一个
     * @param pointer 焦点对象
     * @param popSign 弹层标记
     * @param inGroup 是否限定区域
     * @returns Creater | undefined
     */
    protected rulesTurnUp(pointer: Creater, popSign: string, inGroup?: boolean): Creater | undefined;
    /**
     * 从列表中查找距离基准坐标最近的焦点
     * @param x 基准点X坐标
     * @param y 基准点Y坐标
     * @param list 焦点列表
     * @param popSign 弹层标记
     * @returns Creater | undefined
     */
    protected rulesPosition(x: number, y: number, list: Creater[], popSign: string): Creater | undefined;
    /**
     * 从列表中查找满足条件的焦点
     * @param list 指定焦点列表
     * @param popSign 弹层标记
     * @param callBack 对比条件
     * @returns Creater | undefined
     */
    protected ergodicList(list: Creater[], popSign: string, callBack: (pointer: Creater) => boolean): Creater | undefined;
    /**
     * 变更默认按键值对应关系
     * @param options 自定义按键值
     */
    private cnkiMaps;
    /**
     * 获取事件名称
     * @param keyAction 按键行为
     * @param longFlag 是否为长按
     * @returns eventName
     */
    protected getEventName(keyAction: FocusKeyMaysKeys, longFlag?: boolean): FocusEventName | "";
    /**
     * 获取按键行为
     * @param code 按键Code
     * @returns 按键事件
     */
    protected getKeyAction(code: number): FocusKeyMaysKeys | undefined;
}
