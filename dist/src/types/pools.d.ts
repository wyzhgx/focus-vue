import Creater from './creater';
/**
 * 创建页面/弹窗，焦点信息基础列表
 */
export default class Pools {
    protected groups: {
        [key: string]: Creater;
    };
    items: Creater[];
    pointer?: Creater;
    protected signFlag: {
        [key: string]: boolean;
    };
    protected signItems: {
        [key: string]: Creater[];
    };
    protected groupList: {
        [key: string]: Creater[];
    };
    constructor();
    /**
     * 赋值焦点
     * @param {Creater} creater 焦点对象
     */
    setPointer(creater?: Creater): void;
    /**
     * 检测标记默认选中状态
     * @param {String} sign 节点分组标记名称
     * @returns Boolean
     */
    checkSign(sign: string): boolean;
    /**
     * 获取对应标记列表
     * @param {String} sign 标记名称
     * @returns Array|null
     */
    getSignList(sign: string): Creater[] | null;
    /**
     * 获取标记列表，如果没有则创建新数组
     * @param {String} sign 标记名称
     * @returns Array
     */
    checkOrCreateSignList(sign: string): Creater[];
    getInnerGroupList(group: string): Creater[];
    /**
     * 获取标记列表，如果没有则创建新数组
     * @param {String} sign 标记名称
     * @returns Array
     */
    checkOrCreateGroupList(group: string): Creater[];
    /**
     * 注册焦点
     * @param {Creater} creater 焦点的对象
     */
    registePool(creater: Creater): void;
    /**
     * 移除焦点对象
     * @param {Creater} creater 焦点对象
     */
    splicePool(creater: Creater): void;
}
