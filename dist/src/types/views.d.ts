import Pools from './pools';
import Creater from './creater';
import Service from '../utils/service';
/**
 * 根据路由创建视图区域对象
 */
export default class Views {
    /**
     * 焦点组
     */
    layers: {
        rootPopup: Pools;
        [key: string]: Pools;
    };
    /**
     * 焦点所处弹层标记
     */
    popSign: string;
    /**
     * 当前层级视图组成，默认default，命名视图对应路由名称
     */
    routeViews: {
        [key: string]: {
            [key: string]: Views;
        };
    };
    /**
     * 焦点所处视图标记
     */
    viewSign: string;
    /**
     * 父级视图
     */
    parent?: Views;
    /**
     * 关联元素信息
     */
    __baseView__?: Creater;
    constructor(parent?: Views);
    /**
     * 获取指定弹层对象
     * @param {String} popup 弹层标记名称
     * @returns
     */
    get getLayer(): (popup?: string) => Pools | null;
    /**
     * 获取焦点池对象，如果没有则创建新焦点池
     * @param {String} sign 标记名称
     * @returns Array
     */
    get checkOrCreateLayer(): (popup?: string) => Pools;
    /**
     * 检测默认标记变量
     * @param {String} sign 分组标记名称
     * @param {String} popup 弹层标记名称
     * @returns Boolean
     */
    get checkSignFlag(): (sign: string, popup: string) => boolean;
    /**
     * 注册焦点
     * @param {Object} creater 焦点对象
     */
    get registeLayer(): (creater: Creater) => void;
    /**
     * 注册路由节点对象
     * @param {*} BaseView
     */
    get registeEl(): (routeEl: HTMLElement, routeVpath: string, view: Views, service: Service) => void;
}
