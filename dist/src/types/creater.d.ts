import { DirectiveBinding } from 'vue';
import { FocusBinding } from './focus';
import Pools from './pools';
import Views from './views';
import Service from '../utils/service';
/**
 * 焦点、区域构造类
 */
export default class Creater {
    readonly id: string;
    readonly name: string;
    readonly elem: HTMLElement;
    readonly sign: string;
    readonly group: string;
    readonly popup: string;
    static width: number;
    static height: number;
    static left: number;
    static top: number;
    static posAndSize: DOMRect;
    readonly assign: boolean;
    readonly appoints: boolean;
    readonly routeDepath: number;
    readonly routerEl: HTMLElement;
    readonly routeVpath: string;
    readonly routeViewName: string;
    protected __service?: Service;
    __group__?: Creater;
    __list__?: Creater[];
    __sign__?: Creater[];
    __pools__?: Pools;
    __views__?: Views;
    data: any;
    selected: boolean;
    blur?: (pointer?: Creater) => void;
    focus?: (pointer?: Creater) => void;
    mouseover?: (event?: Event) => void;
    click?: (event?: Event) => void;
    constructor(name: string, elem: HTMLElement, binding: FocusBinding, service: Service);
    get left(): number;
    get top(): number;
    get width(): number;
    get height(): number;
    /**
     * 获取元素相对视口的位置/尺寸信息
     */
    get posAndSize(): DOMRect;
    /**
     * 获取元素dataset内容
     * @param {HTMLElement|Element} elem html元素节点
     * @param {Object} binding 自定义指令绑定的对象
     * @param {Object} context 虚拟节点中的context
     * @returns Object
     */
    createDataSet(elem: HTMLElement, name: string): {
        id: string;
        routeDepath: number;
        routeVpath: string;
        routerEl: HTMLElement;
        routeViewName: string;
    };
    /**
     * 更新焦点
     * @param {HTMLElement|Element} elem 元素节点
     * @param {Object} binding 自定义指令绑定的对象
     * @param {Vnode} vnode 虚拟节点
     */
    update(binding: DirectiveBinding): void;
    /**
     * 兼容动态类名显示异常
     * @param {String} str 焦点/选中类名
     */
    add(str: string): void;
    /**
     * 兼容动态类名显示异常
     * @param {String} str 焦点/选中类名
     */
    updated(): void;
    /**
     * 兼容动态类名显示异常
     * @param {String} str 焦点/选中类名
     */
    remove(str: string): void;
}
