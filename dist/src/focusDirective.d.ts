import { Directive } from 'vue';
import Service from './utils/service';
export declare const createFocusDirective: (services: Service, name: "items" | "group") => Directive;
