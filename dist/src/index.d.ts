import { App } from 'vue';
import keepAliveStore from './utils/store';
import { OptionType } from './types/focus';
declare const _default: (app: App, options?: OptionType) => void;
export default _default;
export { keepAliveStore };
