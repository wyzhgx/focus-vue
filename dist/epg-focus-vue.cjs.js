'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var vue = require('vue');
var vueRouter = require('vue-router');
var pinia = require('pinia');

/**
 * 设置元素属性
 * @param el 元素节点
 * @param name 名称
 * @param value 值
 */
const setDataSet = (el, name, value) => {
    let reg = new RegExp('([A-Z])', 'g');
    name = 'data-' + name.replace(reg, (match, letter) => '-' + letter.toLowerCase());
    el.setAttribute(name, value || '');
};
/**
 * 获取节点路由区域信息
 * @param elem 元素节点
 * @returns 焦点路由相关信息
 */
const checkRouteViewDepath = (elem) => {
    let currentNode = elem.__vueParentComponent;
    let routeDepath = 0;
    let routeVpath = '';
    let routeViewName = "default";
    let routerEl = document.documentElement;
    let list = [];
    while (currentNode) {
        if (currentNode.type.name === 'RouterView') {
            if (!list.length) {
                routerEl = currentNode.vnode.el;
                routeViewName = currentNode.props.name;
            }
            list.unshift(parseStr(currentNode.props.name));
            routeDepath++;
        }
        currentNode = currentNode === null || currentNode === void 0 ? void 0 : currentNode.parent;
    }
    list.unshift('public');
    routeVpath = list.join(':::');
    return { routeDepath, routeVpath, routerEl, routeViewName };
};
/**
 * 计算元素节点位置信息
 * @param el DOM节点
 * @param key 方向
 * @returns number
 */
const getOffsetParent = (el, key) => {
    let offset = 0;
    let parentNode = el;
    var offsetFStr = ('offset' + key.replace(/^([a-z])(.+)/, (match, a, b) => a.toUpperCase() + b));
    do {
        offset += parentNode[offsetFStr];
        if (parentNode.offsetParent instanceof HTMLElement) {
            parentNode = parentNode.offsetParent;
        }
        else {
            break;
        }
    } while (parentNode);
    return offset;
};
/**
 * 将undefined、null等转化为""
 * @param str 字符串
 * @returns string
 */
const parseStr = (str) => {
    if (!str || str === 'undefined' || str === 'null') {
        return '';
    }
    return str;
};
/**
 * 时间单位转换，单位毫秒
 * @param time 时间
 * @returns number
 */
const convertUnits = (time) => {
    if (typeof time === "number") {
        return time;
    }
    else if (typeof time === "string") {
        let tempStr = time;
        if (/^\d*[hH]$/.test(tempStr)) {
            return parseInt(tempStr.replace(/[hH]/, '')) * 60 * 60 * 1000;
        }
        else if (/^\d*[mM]$/.test(tempStr)) {
            return parseInt(tempStr.replace(/[mM]/, '')) * 60 * 1000;
        }
        else if (/^\d*[sS]$/.test(tempStr)) {
            return parseInt(tempStr.replace(/[sS]/, '')) * 1000;
        }
        else if (/^\d*$/.test(tempStr)) {
            return parseInt(tempStr);
        }
    }
    console.warn('时长设置无效或未知单位!');
    return 0;
};
/**
 * 获取弹窗标记
 * @param popup 弹窗标记
 * @returns 弹窗名称
 */
const getPopupStr = (popup) => {
    return popup ? popup === "rootPopup" ? "rootPopup" : 'popup_' + popup : 'rootPopup';
};

/**
 * 焦点、区域构造类
 */
class Creater {
    constructor(name, elem, binding, service) {
        Object.defineProperty(this, "id", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "name", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "elem", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "sign", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: ''
        });
        Object.defineProperty(this, "group", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: ''
        });
        Object.defineProperty(this, "popup", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: ''
        });
        Object.defineProperty(this, "assign", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: false
        });
        Object.defineProperty(this, "appoints", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: false
        });
        Object.defineProperty(this, "routeDepath", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "routerEl", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "routeVpath", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "routeViewName", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: "default"
        });
        Object.defineProperty(this, "__service", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "__group__", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "__list__", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "__sign__", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "__pools__", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "__views__", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "data", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "selected", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: false
        });
        Object.defineProperty(this, "blur", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "focus", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "mouseover", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "click", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.name = name;
        this.elem = elem;
        this.__service = service;
        elem._$FocusCreater = this;
        let { value, modifiers } = binding;
        if (modifiers && Object.keys(modifiers).length) {
            let { assign, appoints } = modifiers;
            this.assign = !!assign;
            this.appoints = !!appoints;
            let { def, sign, group, popup, blur, focus, mouseover, click } = value;
            this.data = def;
            this.sign = sign;
            this.group = group;
            this.popup = popup;
            this.blur = blur;
            this.focus = focus;
            this.mouseover = mouseover;
            this.click = click;
        }
        else {
            this.data = value;
        }
        // 构建焦点
        let { id, routeDepath, routerEl, routeVpath, routeViewName } = this.createDataSet(elem, name);
        this.id = id;
        this.routeDepath = routeDepath;
        this.routerEl = routerEl;
        this.routeVpath = routeVpath;
        this.routeViewName = routeViewName;
    }
    // 元素X方向位置
    get left() {
        return getOffsetParent(this.elem, 'left');
    }
    // 元素Y方向位置
    get top() {
        return getOffsetParent(this.elem, 'top');
    }
    // 元素宽度
    get width() {
        return this.elem.offsetWidth;
    }
    // 元素高度
    get height() {
        return this.elem.offsetHeight;
    }
    /**
     * 获取元素相对视口的位置/尺寸信息
     */
    get posAndSize() {
        return this.elem.getBoundingClientRect();
    }
    /**
     * 获取元素dataset内容
     * @param {HTMLElement|Element} elem html元素节点
     * @param {Object} binding 自定义指令绑定的对象
     * @param {Object} context 虚拟节点中的context
     * @returns Object
     */
    createDataSet(elem, name) {
        let id = name + '-' + (Math.random() + Date.now()).toString(36).replace(/\./, '');
        setDataSet(elem, id);
        let { routeDepath, routeVpath, routerEl, routeViewName } = checkRouteViewDepath(elem);
        return { id, routeDepath, routeVpath, routerEl, routeViewName };
    }
    /**
     * 更新焦点
     * @param {HTMLElement|Element} elem 元素节点
     * @param {Object} binding 自定义指令绑定的对象
     * @param {Vnode} vnode 虚拟节点
     */
    update(binding) {
        let { value, modifiers } = binding;
        if (modifiers && Object.keys(modifiers).length) {
            let { def, blur, focus, mouseover, click } = value;
            this.data = def;
            this.blur = blur;
            this.focus = focus;
            this.mouseover = mouseover;
            this.click = click;
        }
        else {
            this.data = value;
        }
        this.updated();
    }
    /**
     * 兼容动态类名显示异常
     * @param {String} str 焦点/选中类名
     */
    add(str) {
        !this.elem._$FocusClass && (this.elem._$FocusClass = new Set());
        this.elem._$FocusClass.add(str);
        this.elem.classList.add(str);
    }
    /**
     * 兼容动态类名显示异常
     * @param {String} str 焦点/选中类名
     */
    updated() {
        if (this.elem._$FocusClass) {
            this.elem._$FocusClass.size &&
                [...this.elem._$FocusClass].forEach((item) => {
                    this.elem.classList.add(item);
                });
        }
    }
    /**
     * 兼容动态类名显示异常
     * @param {String} str 焦点/选中类名
     */
    remove(str) {
        var _a;
        (_a = this.elem._$FocusClass) === null || _a === void 0 ? void 0 : _a.delete(str);
        this.elem.classList.contains(str) && this.elem.classList.remove(str);
    }
}

const createFocusDirective = (services, name) => ({
    mounted(el, binding) {
        services.register(new Creater(name, el, binding, services));
    },
    updated(el, binding) {
        services.updater(el, binding);
    },
    beforeUnmount(el) {
        services.remover(el);
    }
});

var version = "3.0.1";

const toString = Object.prototype.toString;
/**
 * 判断是否为对象类型
 * @param obj 任意数据
 * @returns boolean
 */
const isObject = (obj) => toString.call(obj) === '[object Object]';
/**
 * 判断是否为字符串类型
 * @param str 任意数据
 * @returns boolean
 */
const isString = (str) => toString.call(str) === '[object String]';
/**
 * 判断是否为函数类型
 * @param fun 任意数据
 * @returns boolean
 */
const isFunction = (fun) => toString.call(fun) === '[object Function]';
/**
 * 判断是否为数字类型
 * @param num 任意数据
 * @returns boolean
 */
const isNumber = (num) => toString.call(num) === '[object Number]';
/**
 * 判断对象是否含有对应的字段
 * @param obj 对象
 * @param prop 字段名称
 * @returns boolean
 */
const hasOwn = (obj, prop) => Object.prototype.hasOwnProperty.call(obj, prop);

/**
 * 创建页面/弹窗，焦点信息基础列表
 */
class Pools {
    constructor() {
        Object.defineProperty(this, "groups", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "items", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: []
        });
        Object.defineProperty(this, "pointer", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "signFlag", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "signItems", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "groupList", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.groups = {};
        this.signFlag = {};
        this.signItems = {};
        this.groupList = {};
    }
    /**
     * 赋值焦点
     * @param {Creater} creater 焦点对象
     */
    setPointer(creater) {
        this.pointer = creater;
    }
    /**
     * 检测标记默认选中状态
     * @param {String} sign 节点分组标记名称
     * @returns Boolean
     */
    checkSign(sign) {
        return this.signFlag[sign] || false;
    }
    /**
     * 获取对应标记列表
     * @param {String} sign 标记名称
     * @returns Array|null
     */
    getSignList(sign) {
        return hasOwn(this.signItems, sign) ? this.signItems[sign] : null;
    }
    /**
     * 获取标记列表，如果没有则创建新数组
     * @param {String} sign 标记名称
     * @returns Array
     */
    checkOrCreateSignList(sign) {
        return hasOwn(this.signItems, sign) ? this.signItems[sign] : (this.signItems[sign] = []);
    }
    getInnerGroupList(group) {
        return hasOwn(this.groupList, group) ? this.groupList[group] : [];
    }
    /**
     * 获取标记列表，如果没有则创建新数组
     * @param {String} sign 标记名称
     * @returns Array
     */
    checkOrCreateGroupList(group) {
        return hasOwn(this.groupList, group) ? this.groupList[group] : (this.groupList[group] = []);
    }
    /**
     * 注册焦点
     * @param {Creater} creater 焦点的对象
     */
    registePool(creater) {
        let { name, sign, group, selected } = creater;
        if (name === 'group') {
            !sign && console.error(`${creater.data}绑定的group区域未添加标识`);
            if (hasOwn(this.groups, sign)) {
                console.warn(`标识为${sign}的group区域已经注册过，请勿重复注册`);
            }
            else {
                this.groups[sign] = creater;
            }
        }
        if (group) {
            // 焦点移入对应分组列表
            const _groupList = this.checkOrCreateGroupList(group);
            _groupList.push(creater);
            creater.__group__ = this.groups[group];
            creater.__list__ = _groupList;
            this.groupList[group].push(creater);
        }
        else {
            // 焦点移入通用焦点列表
            this.items.push(creater);
            creater.__list__ = this.items;
        }
        if (name === 'items' && !!sign) {
            if (this.checkSign(sign)) {
                creater.selected = false;
            }
            else {
                this.signFlag[sign] = selected;
            }
            /**
             * 同步推入相关标记列表
             */
            let signList = this.checkOrCreateSignList(sign);
            signList.push(creater);
            creater.__sign__ = signList;
        }
    }
    /**
     * 移除焦点对象
     * @param {Creater} creater 焦点对象
     */
    splicePool(creater) {
        var _a, _b, _c, _d, _e, _f, _g;
        let { id, name, sign, group } = creater;
        if (name === 'items') {
            // 判断是否为当前pools的焦点
            this.pointer === creater && this.setPointer();
            // 判断是否含有标记，并从对应标记列表中移除
            if (sign) {
                let signIndex = undefined;
                (_a = creater.__sign__) === null || _a === void 0 ? void 0 : _a.forEach((item, key) => {
                    if (item.id === id) {
                        signIndex = key;
                    }
                });
                if (signIndex !== undefined) {
                    (_b = creater.__sign__) === null || _b === void 0 ? void 0 : _b.splice(signIndex, 1);
                }
                this.signFlag[sign] = !!((_c = creater.__sign__) === null || _c === void 0 ? void 0 : _c.filter((item) => item.selected).length);
                if (!((_d = creater.__sign__) === null || _d === void 0 ? void 0 : _d.length)) {
                    // 所有该标记的节点都卸载之后，移除该标记
                    delete this.signFlag[sign];
                    delete this.signItems[sign];
                }
            }
        }
        else if (name === 'group') {
            delete this.groups[sign];
        }
        let listIndex = undefined;
        (_e = creater.__list__) === null || _e === void 0 ? void 0 : _e.forEach((item, key) => {
            if (item.id === id) {
                listIndex = key;
            }
        });
        if (listIndex !== undefined) {
            (_f = creater.__list__) === null || _f === void 0 ? void 0 : _f.splice(listIndex, 1);
        }
        if (!((_g = creater.__list__) === null || _g === void 0 ? void 0 : _g.length) && group) {
            delete this.groupList[sign];
        }
    }
}

/**
 * 根据路由创建视图区域对象
 */
class Views {
    constructor(parent) {
        /**
         * 焦点组
         */
        Object.defineProperty(this, "layers", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        /**
         * 焦点所处弹层标记
         */
        Object.defineProperty(this, "popSign", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        /**
         * 当前层级视图组成，默认default，命名视图对应路由名称
         */
        Object.defineProperty(this, "routeViews", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        /**
         * 焦点所处视图标记
         */
        Object.defineProperty(this, "viewSign", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        /**
         * 父级视图
         */
        Object.defineProperty(this, "parent", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        /**
         * 关联元素信息
         */
        Object.defineProperty(this, "__baseView__", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        this.layers = { rootPopup: new Pools() };
        this.popSign = '';
        this.routeViews = {};
        this.viewSign = '';
        this.parent = parent;
    }
    /**
     * 获取指定弹层对象
     * @param {String} popup 弹层标记名称
     * @returns
     */
    get getLayer() {
        return (popup) => {
            let layer = getPopupStr(popup);
            return this.layers[layer] || null;
        };
    }
    /**
     * 获取焦点池对象，如果没有则创建新焦点池
     * @param {String} sign 标记名称
     * @returns Array
     */
    get checkOrCreateLayer() {
        return (popup) => {
            let layer = getPopupStr(popup);
            if (hasOwn(this.layers, layer)) {
                if (this.layers[layer] instanceof Pools) {
                    return this.layers[layer];
                }
                else {
                    // 可能由于手动修改导致
                    throw `获取${popup}弹层失败`;
                }
            }
            else {
                return (this.layers[layer] = new Pools());
            }
        };
    }
    /**
     * 检测默认标记变量
     * @param {String} sign 分组标记名称
     * @param {String} popup 弹层标记名称
     * @returns Boolean
     */
    get checkSignFlag() {
        return (sign, popup) => {
            let layer = this.getLayer(popup);
            let flag = layer ? layer.checkSign(sign) : false;
            return flag;
        };
    }
    /**
     * 注册焦点
     * @param {Object} creater 焦点对象
     */
    get registeLayer() {
        return (creater) => {
            let layer = this.checkOrCreateLayer(creater.popup);
            creater.__pools__ = layer;
            layer.registePool(creater);
        };
    }
    /**
     * 注册路由节点对象
     * @param {*} BaseView
     */
    get registeEl() {
        return (routeEl, routeVpath, view, service) => {
            // 获取父view对象
            const viewBinding = { value: routeVpath };
            this.__baseView__ = new Creater("views", routeEl, viewBinding, service);
            this.__baseView__.__views__ = view;
        };
    }
}

/**
 * 焦点算法
 */
class Calc {
    constructor(options) {
        // 锁定确认键响应
        Object.defineProperty(this, "blockEnter", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: false
        });
        // 兼容距离计算
        Object.defineProperty(this, "failoverDist", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        // 是否响应keyup按键
        Object.defineProperty(this, "_useKeyUp", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        // 按键方向
        Object.defineProperty(this, "_direction", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: ""
        });
        // 按键响应map
        Object.defineProperty(this, "keyMaps", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: {
                LEFT: [3, 37],
                UP: [1, 38, 61],
                RIGHT: [4, 39],
                DOWN: [2, 40, 77],
                ENTER: [13],
                BACK: [8, 22, 27, 340, 461, 10009],
                NUMBER: [48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105],
                FOWARD: [70, 417],
                REWIND: [82, 412],
                PLAY: [80, 415],
                PAUSE: [81, 19],
                STOP: [83, 413],
                DELETE: [46, 67]
            }
        });
        this.failoverDist = (options === null || options === void 0 ? void 0 : options.failoverDist) || 0;
        this._useKeyUp = (options === null || options === void 0 ? void 0 : options.useKeyUp) || false;
        this.cnkiMaps((options === null || options === void 0 ? void 0 : options.keyMaps) || {});
    }
    // 获取是否开启keyup响应
    get useKeyUp() {
        return this._useKeyUp;
    }
    // 获取按键方向
    get direction() {
        return this._direction;
    }
    get enterKeyCode() {
        return this.keyMaps.ENTER[0];
    }
    /**
     * 计算焦点元素到某点的最短距离
     * @param left 焦点left
     * @param top 焦点top
     * @param width 焦点width
     * @param height 焦点height
     * @param x 基准点x
     * @param y 基准点y
     * @returns number
     */
    rulesSurfaceToPoint(left, top, width, height, x, y) {
        if (x < left) {
            if (y < top) {
                return Math.sqrt(Math.pow(left - x, 2) + Math.pow(top - y, 2));
            }
            else if (y >= top && y <= top + height) {
                return left - x;
            }
            else {
                return Math.sqrt(Math.pow(left - x, 2) + Math.pow(y - top - height, 2));
            }
        }
        else if (x > left + width) {
            if (y < top) {
                return Math.sqrt(Math.pow(x - left - width, 2) + Math.pow(top - y, 2));
            }
            else if (y >= top && y <= top + height) {
                return x - left;
            }
            else {
                return Math.sqrt(Math.pow(x - left - width, 2) + Math.pow(y - top - height, 2));
            }
        }
        else {
            if (y < top) {
                return top - y;
            }
            else if (y >= top && y <= top + height) {
                return 0;
            }
            else {
                return y - top - height;
            }
        }
    }
    /**
     * 判断元素某一方向上是否交错
     * 参数取两个元素同一方向上的起点及相应尺寸
     * @param pb prev元素起点
     * @param pc prev元素相应方向的尺寸
     * @param nb next元素起点
     * @param nc next元素相应方向的尺寸
     * @returns number
     */
    getOverlay(pb, pc, nb, nc) {
        if (nb >= pb + pc || nb + nc <= pb) {
            return 0;
        }
        else {
            let nd = nb + nc;
            let pd = pb + pc;
            if (nd > pd) {
                return Math.min(pd - nb, pc);
            }
            else if (nd === pd) {
                return Math.min(nc, pc);
            }
            else {
                return Math.min(nd - pb, nc);
            }
        }
    }
    /**
     * 对比返回距离x,y坐标更近的焦点
     * @param x 基准点x
     * @param y 基准点y
     * @param target 下一个焦点
     * @param cache 缓存焦点对象
     * @returns Creater | undefined
     */
    rulesPoint(x, y, target, cache) {
        if (cache) {
            let { posAndSize } = target;
            let { top: nt, left: nl, width: nw, height: nh } = posAndSize;
            if (nw && nh) {
                let { top, left, width, height } = cache;
                let nextDist = this.rulesSurfaceToPoint(nl, nt, nw, nh, x, y);
                let prevDist = this.rulesSurfaceToPoint(left, top, width, height, x, y);
                if (nextDist < prevDist) {
                    return target;
                }
                else if (nextDist === prevDist) {
                    if (nt > top || nl > left) {
                        return target;
                    }
                    else {
                        return cache;
                    }
                }
                else {
                    return cache;
                }
            }
            else {
                return cache;
            }
        }
        else {
            return target;
        }
    }
    /**
     * 通用元素焦点移动计算规则
     * @param point 焦点元素对象
     * @param target 待做对比的焦点元素对象
     * @param direction 焦点移动方向
     * @param cache 满足条件的最优的焦点元素对象
     * @returns Creater | undefined
     */
    rules(point, target, direction, cache) {
        let { posAndSize: nextPosAndSize, name, sign, __pools__ } = target;
        let { left: nextLeft, top: nextTop, width: nextWidth, height: nextHeight } = nextPosAndSize;
        if (!nextWidth || !nextHeight) {
            return cache;
        }
        if (name === 'group') {
            // 检测该target为group类型时；如果里边没有节点列表，则跳过
            const list = __pools__ === null || __pools__ === void 0 ? void 0 : __pools__.getInnerGroupList(sign).length;
            if (!list) {
                return cache;
            }
        }
        let temp;
        let { posAndSize } = point;
        let { top, left, width, height } = posAndSize;
        if (direction === 'left') {
            // left值小于焦点元素, 且left、top差值均最小, 且水平方向有交集
            let nextOverlay = this.getOverlay(top, height, nextTop, nextHeight);
            if (nextLeft + nextWidth <= left && nextOverlay) {
                if (cache) {
                    let { posAndSize: prevPosAndSize } = cache;
                    let { left: prevLeft, top: prevTop, height: prevHeight } = prevPosAndSize;
                    if (prevLeft > nextLeft) {
                        temp = cache;
                    }
                    else if (prevLeft === nextLeft) {
                        let prevOverlay = this.getOverlay(top, height, prevTop, prevHeight);
                        if (prevOverlay >= nextOverlay) {
                            temp = cache;
                        }
                        else {
                            temp = target;
                        }
                    }
                    else {
                        temp = target;
                    }
                }
                else {
                    temp = target;
                }
            }
        }
        else if (direction === 'right') {
            // left值大于焦点元素, 且left、top差值均最小, 且水平方向有交集
            let nextOverlay = this.getOverlay(top, height, nextTop, nextHeight);
            if (nextLeft >= left + width && nextOverlay) {
                if (cache) {
                    let { posAndSize: prevPosAndSize } = cache;
                    let { left: prevLeft, top: prevTop, height: prevHeight } = prevPosAndSize;
                    if (prevLeft < nextLeft) {
                        temp = cache;
                    }
                    else if (prevLeft === nextLeft) {
                        let prevOverlay = this.getOverlay(top, height, prevTop, prevHeight);
                        if (prevOverlay >= nextOverlay) {
                            temp = cache;
                        }
                        else {
                            temp = target;
                        }
                    }
                    else {
                        temp = target;
                    }
                }
                else {
                    temp = target;
                }
            }
        }
        else if (direction === 'up') {
            // top值小于焦点元素, 水平方向无交集
            let targetPosition = nextTop + nextHeight;
            if (targetPosition <= top) {
                if (cache) {
                    let { posAndSize: prevPosAndSize } = cache;
                    let { left: prevLeft, top: prevTop, width: prevWidth, height: prevHeight } = prevPosAndSize;
                    let cachePosition = prevTop + prevHeight;
                    let nextOverlay = this.getOverlay(left, width, nextLeft, nextWidth);
                    let prevOverlay = this.getOverlay(left, width, prevLeft, prevWidth);
                    if (nextOverlay > prevOverlay) {
                        if (targetPosition >= cachePosition) {
                            temp = target;
                        }
                        else {
                            if (top - targetPosition <= this.failoverDist) {
                                temp = target;
                            }
                            else {
                                temp = cache;
                            }
                        }
                    }
                    else if (nextOverlay === prevOverlay) {
                        if (cachePosition > targetPosition) {
                            temp = cache;
                        }
                        else if (cachePosition === targetPosition) {
                            if (nextOverlay === 0) {
                                let prevDist = this.rulesSurfaceToPoint(prevLeft, prevTop, prevWidth, prevHeight, prevLeft < left ? left : left + width, top);
                                let nextDist = this.rulesSurfaceToPoint(nextLeft, nextTop, nextWidth, nextHeight, nextLeft < left ? left : left + width, top);
                                if (prevDist >= nextDist) {
                                    temp = target;
                                }
                                else {
                                    temp = cache;
                                }
                            }
                            else {
                                if (prevLeft > nextLeft) {
                                    temp = target;
                                }
                                else {
                                    temp = cache;
                                }
                            }
                        }
                        else {
                            temp = target;
                        }
                    }
                    else {
                        if (cachePosition >= targetPosition) {
                            temp = cache;
                        }
                        else {
                            if (top - cachePosition <= this.failoverDist) {
                                temp = cache;
                            }
                            else {
                                temp = target;
                            }
                        }
                    }
                }
                else {
                    temp = target;
                }
            }
        }
        else if (direction === 'down') {
            // top值大于焦点元素, 水平方向无交集
            if (nextTop >= top + height) {
                if (cache) {
                    let { posAndSize: prevPosAndSize } = cache;
                    let { left: prevLeft, top: prevTop, width: prevWidth, height: prevHeight } = prevPosAndSize;
                    let nextOverlay = this.getOverlay(left, width, nextLeft, nextWidth);
                    let prevOverlay = this.getOverlay(left, width, prevLeft, prevWidth);
                    if (nextOverlay > prevOverlay) {
                        // target交集更大
                        if (nextTop > prevTop) {
                            if (nextTop - top - height <= this.failoverDist) {
                                temp = target;
                            }
                            else {
                                if (nextTop < prevTop + prevHeight) {
                                    temp = target;
                                }
                                else {
                                    temp = cache;
                                }
                            }
                        }
                        else {
                            temp = target;
                        }
                    }
                    else if (nextOverlay === prevOverlay) {
                        if (nextTop > prevTop) {
                            temp = cache;
                        }
                        else if (nextTop === prevTop) {
                            if (nextOverlay === 0) {
                                let prevDist = this.rulesSurfaceToPoint(prevLeft, prevTop, prevWidth, prevHeight, prevLeft < left ? left : left + width, top + height);
                                let nextDist = this.rulesSurfaceToPoint(nextLeft, nextTop, nextWidth, nextHeight, nextLeft < left ? left : left + width, top + height);
                                if (prevDist >= nextDist) {
                                    temp = target;
                                }
                                else {
                                    temp = cache;
                                }
                            }
                            else {
                                if (prevLeft > nextLeft) {
                                    temp = target;
                                }
                                else {
                                    temp = cache;
                                }
                            }
                        }
                        else {
                            temp = target;
                        }
                    }
                    else {
                        // cache交集更大
                        if (prevTop > nextTop) {
                            if (prevTop - top - height <= this.failoverDist) {
                                temp = cache;
                            }
                            else {
                                if (prevTop < nextTop + nextHeight) {
                                    temp = cache;
                                }
                                else {
                                    temp = target;
                                }
                            }
                        }
                        else {
                            temp = cache;
                        }
                    }
                }
                else {
                    temp = target;
                }
            }
        }
        return temp;
    }
    /**
     * 移入Views或group计算规则
     * @param pointer 区域限制对象
     * @param target 待做对比的焦点对象
     * @param direction 焦点移动方向
     * @param cache 满足条件的最优的焦点元素对象
     * @returns Creater | undefined
     */
    rulesDist(pointer, target, direction = 'down', cache) {
        let { posAndSize: nextPosAndSize, name, sign, __pools__ } = target;
        let { left: nextLeft, top: nextTop, width: nextWidth, height: nextHeight } = nextPosAndSize;
        if (!nextWidth && !nextHeight) {
            return cache;
        }
        if (name === 'group') {
            // 检测该target为group类型时；如果里边没有节点列表，则跳过
            const list = __pools__ === null || __pools__ === void 0 ? void 0 : __pools__.getInnerGroupList(sign).length;
            if (!list) {
                return cache;
            }
        }
        let { posAndSize } = pointer;
        let { left, top, width, height } = posAndSize;
        if (nextLeft >= left && nextLeft + nextWidth <= left + width && nextTop >= top && nextTop + nextHeight <= top + height) {
            let temp;
            if (cache) {
                let { posAndSize: prevPosAndSize } = cache;
                let { left: prevLeft, top: prevTop, height: prevHeight } = prevPosAndSize;
                if (direction === 'left') {
                    if (nextTop < prevTop || (nextTop === prevTop && nextLeft > prevLeft)) {
                        temp = target;
                    }
                    else {
                        temp = cache;
                    }
                }
                else if (direction === 'right' || direction === 'down') {
                    if (nextTop < prevTop || (nextTop === prevTop && nextLeft < prevLeft)) {
                        temp = target;
                    }
                    else {
                        temp = cache;
                    }
                }
                else if (direction === 'up') {
                    if (nextTop + nextHeight > prevTop + prevHeight || (nextTop + nextHeight === prevTop + prevHeight && nextLeft < prevLeft)) {
                        temp = target;
                    }
                    else {
                        temp = cache;
                    }
                }
            }
            else {
                temp = target;
            }
            return temp;
        }
        else {
            return cache;
        }
    }
    /**
     * 移入新group的焦点计算方法
     * @param direction 移动方向
     * @param pointer 焦点元素对象
     * @returns Creater | undefined
     */
    rulesInToGroup(direction, pointer) {
        let temp;
        let { __pools__, sign } = pointer;
        let list = (__pools__ === null || __pools__ === void 0 ? void 0 : __pools__.getInnerGroupList(sign)) || [];
        if (list.length) {
            list.forEach((val) => {
                temp = this.rulesDist(pointer, val, direction, temp);
            });
            if ((temp === null || temp === void 0 ? void 0 : temp.name) === 'group') {
                return this.rulesInToGroup(direction, temp);
            }
        }
        return temp;
    }
    /**
     * 移入新Views的焦点计算方法
     * @param direction 方向
     * @param views 对应Views对象
     * @param popSign 弹层标记
     * @returns Creater | undefined
     */
    rulesInToViews(direction, views, popSign) {
        var _a;
        let temp;
        let { viewSign, routeViews, __baseView__ } = views;
        let childViews = [];
        if (viewSign && routeViews[viewSign]) {
            for (const val of Object.values(routeViews[viewSign])) {
                val.__baseView__ && childViews.push(val.__baseView__);
            }
        }
        let tempArr = (((_a = views.getLayer(popSign)) === null || _a === void 0 ? void 0 : _a.items) || []).concat(childViews);
        if (tempArr.length && __baseView__) {
            tempArr.forEach((val) => {
                temp = this.rulesDist(__baseView__, val, direction, temp);
            });
            if ((temp === null || temp === void 0 ? void 0 : temp.name) === 'views') {
                return this.rulesInToViews(direction, temp === null || temp === void 0 ? void 0 : temp.__views__, popSign);
            }
            else if ((temp === null || temp === void 0 ? void 0 : temp.name) === 'group') {
                return this.rulesInToGroup(direction, temp);
            }
        }
        return temp;
    }
    /**
     * 查找相邻节点规则,如果含有group标记，则仅在对应group内查找相邻节点
     * @param direction 移动方向
     * @param pointer 焦点元素对象
     * @param inGroup 是否限定在路由区域内移动
     * @param popSign 弹层标记
     * @returns Creater | undefined
     */
    rulesInArea(direction, pointer, inGroup = false, popSign = "") {
        var _a, _b;
        let next;
        let { __views__, __group__, name } = pointer;
        let { viewSign, routeViews, parent } = __views__;
        let parentList = [];
        if (name === 'views') {
            let layerList = [];
            if (parent !== undefined) {
                if (parent.viewSign && parent.routeViews[parent.viewSign]) {
                    for (const val of Object.values(parent.routeViews[parent.viewSign])) {
                        val.__baseView__ && layerList.push(val.__baseView__);
                    }
                }
            }
            parentList = layerList.concat(((_a = parent === null || parent === void 0 ? void 0 : parent.getLayer(popSign)) === null || _a === void 0 ? void 0 : _a.items) || []);
        }
        else {
            // 获取焦点子view代理对象
            let childViews = [];
            if (viewSign && routeViews[viewSign]) {
                for (const val of Object.values(routeViews[viewSign])) {
                    val.__baseView__ && childViews.push(val.__baseView__);
                }
            }
            parentList = (((_b = __views__ === null || __views__ === void 0 ? void 0 : __views__.getLayer(popSign)) === null || _b === void 0 ? void 0 : _b.items) || []).concat(childViews);
        }
        if (parentList.length) {
            parentList.forEach((value) => {
                if (value !== pointer) {
                    next = this.rules(pointer, value, direction, next) || next;
                }
            });
            if (next !== undefined) {
                if (next.name === 'views') {
                    if (!inGroup) {
                        // 移入进子路由中，移动到对应方向上的第一个可见节点上
                        return this.rulesInToViews(direction, next.__views__, popSign);
                    }
                }
                else if (next.name === 'group') {
                    return this.rulesInToGroup(direction, next);
                }
                else if (next.name === 'items') {
                    return next;
                }
            }
            else if (!inGroup) {
                // 移出区域
                let innerParent, viewCreater, innerView;
                if (name === 'views') {
                    innerView = __views__ === null || __views__ === void 0 ? void 0 : __views__.parent;
                }
                else {
                    innerView = __views__;
                }
                innerParent = innerView === null || innerView === void 0 ? void 0 : innerView.parent;
                viewCreater = innerView === null || innerView === void 0 ? void 0 : innerView.__baseView__;
                if (__group__) {
                    return this.rulesInArea(direction, __group__, inGroup, popSign);
                }
                else if (innerParent) {
                    return this.rulesInArea(direction, viewCreater, inGroup, popSign);
                }
            }
        }
        return next;
    }
    /**
     * 查找指定方向上与pointer含有相同标记的下一个焦点
     * @param direction 方向
     * @param pointer 焦点对象
     * @returns Creater | undefined
     */
    rulesInSign(direction, pointer) {
        var _a, _b;
        let temp;
        if ((_a = pointer.__sign__) === null || _a === void 0 ? void 0 : _a.length) {
            (_b = pointer.__sign__) === null || _b === void 0 ? void 0 : _b.forEach((val) => {
                val.selected = false;
                if (val != pointer) {
                    temp = this.rules(pointer, val, direction, temp) || temp;
                }
            });
            if (temp) {
                temp.selected = true;
            }
            else {
                pointer.selected = true;
            }
        }
        return temp;
    }
    /**
     * 换到下一行第一个
     * @param pointer 焦点对象
     * @param popSign 弹层标记
     * @param inGroup 是否限定区域
     * @returns Creater | undefined
     */
    rulesTurnDown(pointer, popSign, inGroup = false) {
        let { posAndSize } = pointer;
        let { top, height } = posAndSize;
        let temper = this.rulesInArea('down', pointer, inGroup, popSign);
        if (temper) {
            let tempObj, flag = false;
            do {
                tempObj = this.rulesInArea('left', temper, inGroup, popSign);
                if (tempObj) {
                    let { posAndSize } = tempObj;
                    let { top: nextTop } = posAndSize;
                    flag = nextTop >= top + height;
                }
                else {
                    flag = false;
                }
            } while (tempObj && flag && (temper = tempObj));
        }
        return temper;
    }
    /**
     * 换到上一行最后一个
     * @param pointer 焦点对象
     * @param popSign 弹层标记
     * @param inGroup 是否限定区域
     * @returns Creater | undefined
     */
    rulesTurnUp(pointer, popSign, inGroup = false) {
        let { posAndSize } = pointer;
        let { top } = posAndSize;
        let temper = this.rulesInArea('up', pointer, inGroup, popSign);
        if (temper) {
            let tempObj, flag = false;
            do {
                tempObj = this.rulesInArea('right', temper, inGroup, popSign);
                if (tempObj) {
                    let { posAndSize } = tempObj;
                    let { top: nextTop, height: nextHeight } = posAndSize;
                    flag = nextTop + nextHeight <= top;
                }
                else {
                    flag = false;
                }
            } while (tempObj && flag && (temper = tempObj));
        }
        return temper;
    }
    /**
     * 从列表中查找距离基准坐标最近的焦点
     * @param x 基准点X坐标
     * @param y 基准点Y坐标
     * @param list 焦点列表
     * @param popSign 弹层标记
     * @returns Creater | undefined
     */
    rulesPosition(x, y, list, popSign) {
        var _a, _b;
        let temp;
        if (list.length) {
            list.forEach((val) => {
                temp = this.rulesPoint(x, y, val, temp);
            });
            let { __views__ } = temp;
            // 获取焦点子view代理对象
            let { viewSign, routeViews } = __views__;
            if ((temp === null || temp === void 0 ? void 0 : temp.name) === 'views') {
                let childViews = [];
                if (viewSign && routeViews[viewSign]) {
                    for (const val of Object.values(routeViews[viewSign])) {
                        val.__baseView__ && childViews.push(val.__baseView__);
                    }
                }
                let tempArr = (((_a = __views__ === null || __views__ === void 0 ? void 0 : __views__.getLayer(popSign)) === null || _a === void 0 ? void 0 : _a.items) || []).concat(childViews);
                return this.rulesPosition(x, y, tempArr, popSign);
            }
            else if ((temp === null || temp === void 0 ? void 0 : temp.name) === 'group') {
                return this.rulesPosition(x, y, ((_b = __views__ === null || __views__ === void 0 ? void 0 : __views__.getLayer(popSign)) === null || _b === void 0 ? void 0 : _b.getInnerGroupList(temp === null || temp === void 0 ? void 0 : temp.sign)) || [], popSign);
            }
        }
        return temp;
    }
    /**
     * 从列表中查找满足条件的焦点
     * @param list 指定焦点列表
     * @param popSign 弹层标记
     * @param callBack 对比条件
     * @returns Creater | undefined
     */
    ergodicList(list, popSign, callBack) {
        var _a, _b;
        let temp;
        for (let i = 0; i < list.length; i++) {
            const item = list[i];
            let { __views__, sign, name } = item;
            // 获取焦点子view代理对象
            let { viewSign, routeViews } = __views__;
            if (name === 'views') {
                let childViews = [];
                if (viewSign && routeViews[viewSign]) {
                    for (const val of Object.values(routeViews[viewSign])) {
                        val.__baseView__ && childViews.push(val.__baseView__);
                    }
                }
                let tempArr = (((_a = __views__ === null || __views__ === void 0 ? void 0 : __views__.getLayer(popSign)) === null || _a === void 0 ? void 0 : _a.items) || []).concat(childViews);
                temp = this.ergodicList(tempArr, popSign, callBack);
            }
            else if (name === 'group') {
                temp = this.ergodicList(((_b = __views__ === null || __views__ === void 0 ? void 0 : __views__.getLayer(popSign)) === null || _b === void 0 ? void 0 : _b.getInnerGroupList(sign)) || [], popSign, callBack);
            }
            else if (name === 'items') {
                if (callBack(item)) {
                    temp = item;
                }
            }
            if (temp) {
                break;
            }
        }
        return temp;
    }
    /**
     * 变更默认按键值对应关系
     * @param options 自定义按键值
     */
    cnkiMaps(options) {
        for (let [mapKey, mapVal] of Object.entries(options)) {
            let cnkiKey = mapKey.toUpperCase();
            if (hasOwn(this.keyMaps, cnkiKey)) {
                let mapList;
                if (isNumber(mapVal)) {
                    mapList = [mapVal];
                }
                else if (mapVal instanceof Array) {
                    mapList = mapVal;
                }
                else {
                    continue;
                }
                mapList.forEach((val) => {
                    for (let [key, list] of Object.entries(this.keyMaps)) {
                        let index = list.indexOf(val);
                        let prop = key;
                        if (prop === cnkiKey && index < 0) {
                            this.keyMaps[prop].push(val);
                        }
                        else if (prop !== cnkiKey && index >= 0) {
                            this.keyMaps[prop].splice(index, 1);
                        }
                    }
                });
            }
        }
    }
    /**
     * 获取事件名称
     * @param keyAction 按键行为
     * @param longFlag 是否为长按
     * @returns eventName
     */
    getEventName(keyAction, longFlag) {
        this._direction = ["LEFT", "RIGHT", "UP", "DOWN"].includes(keyAction) ? keyAction.toLocaleLowerCase() : "";
        if (keyAction === 'ENTER') {
            if (this.blockEnter) {
                this.blockEnter = false;
                return "";
            }
            else {
                this.blockEnter = true;
            }
        }
        else {
            this.blockEnter = false;
        }
        return 'key' + (longFlag ? 'Long' : '') + keyAction.toLowerCase().replace(/^([a-z])(.+)/, (match, a, b) => a.toUpperCase() + b);
    }
    /**
     * 获取按键行为
     * @param code 按键Code
     * @returns 按键事件
     */
    getKeyAction(code) {
        for (const [key, value] of Object.entries(this.keyMaps)) {
            if (value.includes(code)) {
                return key;
            }
        }
    }
}

class MoveRule extends Calc {
    constructor(options) {
        super(options);
        Object.defineProperty(this, "_views", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: new Views()
        }); // 视图区域
        Object.defineProperty(this, "_path", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: []
        }); // 路由匹配路径列表
        Object.defineProperty(this, "selectName", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: 'selected'
        });
        Object.defineProperty(this, "focusName", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: 'focushover'
        });
        Object.defineProperty(this, "popSignList", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: []
        });
        this.selectName = (options === null || options === void 0 ? void 0 : options.selectName) || "selected";
        this.focusName = (options === null || options === void 0 ? void 0 : options.focusName) || "focushover";
    }
    get pointer() {
        let pointer;
        this.ergodicViews((view) => {
            let layer = view.getLayer(this.popSign);
            if (layer && layer.pointer) {
                pointer = layer.pointer;
            }
            return !!pointer;
        });
        return pointer;
    }
    get popSign() {
        return this.popSignList.length ? this.popSignList[0] : "";
    }
    /**
     * 获取焦点所属Views
     * @param routeDepath 节点路由深度
     * @param routeVpath 节点路由路径
     * @returns
     */
    getView(routeDepath, routeVpath) {
        if (routeDepath === 0) {
            return this._views;
        }
        else {
            let list = this._path;
            let pathArr = routeVpath.split(':::').filter((item) => item !== 'public');
            let views = this._views;
            for (let i = 0; i < routeDepath; i++) {
                views = views.routeViews[list[i]][pathArr[i]];
            }
            return views;
        }
    }
    /**
     * 根据路由创建视图层，并变更当前路径对象
     * @param {RouteLocationNormalized} to VUE路由守卫中的目标
     */
    get createView() {
        return (to) => {
            // 未匹配到路由信息，不创建视图层
            if (!to.matched.length) {
                return false;
            }
            let parentViews = { default: this._views };
            let tempPath = [];
            // 根据匹配到的路由信息创建视图层
            to.matched.forEach((value) => {
                let name = parseStr(value.name);
                if (name) {
                    if (value.components) {
                        let parentRouteViews = parentViews['default'].routeViews;
                        for (let propName of Object.keys(value.components)) {
                            parentRouteViews = parentViews[hasOwn(parentViews, propName) ? propName : 'default'].routeViews;
                            if (!hasOwn(parentRouteViews, name)) {
                                parentRouteViews[name] = {};
                            }
                            if (!hasOwn(parentRouteViews[name], propName)) {
                                parentRouteViews[name][propName] = new Views(parentViews[hasOwn(parentViews, propName) ? propName : 'default']);
                            }
                            parentViews[hasOwn(parentViews, propName) ? propName : 'default'].viewSign = name;
                        }
                        parentViews = parentRouteViews[name];
                    }
                    else {
                        let parentRouteViews = parentViews['default'].routeViews;
                        if (!hasOwn(parentRouteViews, name)) {
                            parentRouteViews[name] = {};
                        }
                        if (!hasOwn(parentRouteViews[name], 'default')) {
                            parentRouteViews[name]['default'] = new Views(parentViews['default']);
                        }
                        parentViews['default'].viewSign = name;
                        parentViews = parentRouteViews[name];
                    }
                    tempPath.push(name);
                }
                else {
                    throw new Error(`路由：${value.path}，未指定name属性`);
                }
            });
            this._path = tempPath;
        };
    }
    /**
     * 获取指定popup弹层基础焦点列表
     * @param createrOrPopup 焦点对象或弹层标记
     * @returns Creater[]
     */
    getList(createrOrPopup) {
        if (createrOrPopup instanceof Creater) {
            const creater = createrOrPopup;
            return creater.__list__ || [];
        }
        else if (typeof createrOrPopup === "string") {
            const popup = createrOrPopup;
            let list = [];
            // 获取指定弹层或当前页面焦点列表
            this.ergodicViews((value) => {
                let layer = value.getLayer(popup);
                if (layer) {
                    list = layer.items;
                    return true;
                }
                return false;
            });
            return list;
        }
        return [];
    }
    /**
     * 按当前路径遍历Views对象
     * @param func 视图匹配方法
     * @param views 基础视图
     * @returns
     */
    ergodicViews(func, views = this._views) {
        let flag = func(views);
        if (!flag) {
            let parentViewsArr = [views];
            for (let index = 0; index < this._path.length; index++) {
                let prop = this._path[index];
                let arr = [];
                parentViewsArr.forEach((value) => {
                    if (hasOwn(value.routeViews, prop)) {
                        arr.push(...Object.values(value.routeViews[prop]));
                    }
                });
                parentViewsArr = arr;
                for (let item = 0; item < arr.length; item++) {
                    if (func(arr[item])) {
                        flag = true;
                        return flag;
                    }
                }
            }
        }
        return flag;
    }
    /**
     * 打开新的弹层
     * @param popup 弹层标记
     * @param callBack 切换弹层后的会调，可能未获焦
     */
    get openPop() {
        return (popup, callBack) => {
            let flag = true;
            const popSignStr = getPopupStr(popup);
            this.ergodicViews((view) => {
                view.popSign = '';
                if (popup !== undefined && flag && view.getLayer(popup)) {
                    view.popSign = popup;
                    flag = false;
                }
                return !flag;
            });
            popup ? !flag && getPopupStr(this.popSign) !== popSignStr && this.popSignList.unshift(popup) : (this.popSignList = []);
            callBack && isFunction(callBack) ? callBack() : !this.pointer && this.initFocus(0, 0);
        };
    }
    /**
     * 关闭弹窗
     */
    get closePop() {
        return () => {
            this.popSignList.shift();
            this.openPop(this.popSignList[0]);
        };
    }
    /**
     * 设置焦点
     * @param next 焦点对象
     * @returns Creater | undefined
     */
    get checkFocus() {
        return (next) => {
            var _a, _b, _c;
            let prev = this.pointer;
            if (next !== undefined && next !== prev) {
                if (prev !== undefined) {
                    prev.remove(this.focusName);
                    (_a = prev.__pools__) === null || _a === void 0 ? void 0 : _a.setPointer();
                    if (typeof prev.blur === "function") {
                        prev.blur(prev);
                    }
                }
                if (next.sign) {
                    if (prev && next.sign === prev.sign) {
                        (_b = next.__sign__) === null || _b === void 0 ? void 0 : _b.forEach((value) => {
                            value.selected = value === next;
                            // 添加移除选中类名
                            value.selected ? value.add(this.selectName) : value.remove(this.selectName);
                        });
                    }
                    else {
                        next = this.getSign(next);
                    }
                }
                next === null || next === void 0 ? void 0 : next.add(this.focusName);
                (_c = next === null || next === void 0 ? void 0 : next.__pools__) === null || _c === void 0 ? void 0 : _c.setPointer(next);
                if (typeof (next === null || next === void 0 ? void 0 : next.focus) === "function") {
                    next === null || next === void 0 ? void 0 : next.focus(next);
                }
                return next;
            }
        };
    }
    /**
     * 焦点移动方法
     * @param direction 移动方向（left,right,up,down）
     * @param pointer 焦点对象
     * @returns Creater | undefined
     */
    get moveFocus() {
        return (direction, pointer = this.pointer) => {
            // 判断当前有焦点时再移动
            return pointer ? this.checkFocus(this.rulesInArea(direction, pointer, false, this.popSign)) : this.checkPosition(0, 0);
        };
    }
    /**
     * 根据位置信息设置焦点
     * @param left 元素X方向偏移量
     * @param top 元素Y方向偏移量
     * @param callBack 设置焦点后需要执行的方法(非必传)
     * @returns Creater | undefined
     */
    get checkPosition() {
        return (left, top, callBack) => {
            let tempArr = this.getList(this.popSign);
            let temp = this.rulesPosition(left, top, tempArr, this.popSign);
            if (temp && temp.sign) {
                temp = this.getSign(temp);
            }
            this.checkFocus(temp);
            callBack && isFunction(callBack) && callBack(this.pointer);
            return this.pointer;
        };
    }
    /**
     * 自定义条件初始化焦点
     * @param check 焦点筛选条件方法，并返回Boolean值
     * @param callBack 非必传字段、初始化焦点后需要执行的方法
     */
    get createFocus() {
        return (check, callBack) => {
            let tempArr = this.getList(this.popSign);
            let temp = this.ergodicList(tempArr, this.popSign, check);
            if (temp === null || temp === void 0 ? void 0 : temp.sign) {
                temp = this.getSign(temp);
            }
            this.checkFocus(temp);
            callBack && isFunction(callBack) && callBack(this.pointer);
        };
    }
    /**
     * 通用焦点初始化方法
     * @param a 焦点X方向偏移量/焦点筛选条件方法
     * @param b 焦点Y方向偏移量/焦点初始化后的回调
     * @param c 焦点初始化后的回调
     */
    get initFocus() {
        return (a, b, c) => {
            vue.nextTick(() => {
                if (typeof a === "function") {
                    this.createFocus(a, b && typeof b === "function" ? b : undefined);
                }
                else if (typeof a === "number" && typeof b === "number") {
                    this.checkPosition(a, b, c);
                }
                else if (a instanceof Creater) {
                    if (a.name === 'items') {
                        this.checkFocus(a);
                        b && typeof b === "function" && b(a);
                    }
                }
            });
        };
    }
    /**
     * 下一个焦点，仅处理当前弹层水平方向的下一个
     * @param flag 是否在限定区域内移动
     * @param callBack 触及边界且换行成功后执行的方法
     * @returns Creater | undefined
     */
    get next() {
        return (flag, callBack) => {
            let pointer;
            if (this.pointer) {
                pointer = this.rulesInArea('right', this.pointer, flag, this.popSign);
                if (pointer) {
                    // 未到边界继续执行向右的逻辑
                    this.checkFocus(pointer);
                }
                else {
                    // 移动到边界后计算下一行第一个元素
                    pointer = this.rulesTurnDown(this.pointer, this.popSign, flag);
                    pointer && this.checkFocus(pointer) && isFunction(callBack) && callBack(this.pointer);
                }
            }
            else {
                this.checkPosition(0, 0);
                flag && this.pointer && isFunction(callBack) && callBack(this.pointer);
            }
            return pointer;
        };
    }
    /**
     * 上一个焦点，仅处理当前弹层水平方向的上一个
     * @param flag 是否在限定区域内移动
     * @param callBack 触及边界且换行成功后执行的方法
     * @returns Creater | undefined
     */
    get prev() {
        return (flag, callBack) => {
            let prev = this.pointer;
            let next;
            if (prev) {
                next = this.rulesInArea('left', prev, flag, this.popSign);
                if (next) {
                    // 未到边界继续执行向右的逻辑
                    this.checkFocus(next);
                }
                else {
                    // 移动到边界后计算上一行最后一个元素
                    next = this.rulesTurnUp(prev, this.popSign, flag);
                    next && this.checkFocus(next) && isFunction(callBack) && callBack();
                }
            }
            else {
                next = this.checkPosition(0, 0);
                flag && next && isFunction(callBack) && callBack();
            }
            return next;
        };
    }
    /**
     * 移动到当前焦点所在行的第一个|移动标记到焦点所在行的第一个
     * @param createrOrFlag 是否限定区域|含有sign标记的焦点对象
     * @returns Creater | undefined
     */
    get start() {
        return (createrOrFlag) => {
            if (isObject(createrOrFlag)) {
                const creater = createrOrFlag;
                if (creater.sign) {
                    let next = creater;
                    let prev = creater;
                    while ((prev = this.rulesInSign('left', prev))) {
                        next = prev;
                    }
                    if (next !== creater) {
                        creater.remove(this.selectName);
                        next.add(this.selectName);
                    }
                    return next;
                }
                else {
                    console.error(`您当前选择的焦点对象，未含有sign标记`);
                }
            }
            else {
                if (this.pointer) {
                    const flag = createrOrFlag;
                    let target = this.pointer;
                    let tempObj;
                    // 仅在焦点标记的区域、或默认全局移动到左侧边界
                    while ((tempObj = this.rulesInArea('left', target, flag, this.popSign))) {
                        target = tempObj;
                    }
                    // 移动到不同标记的焦点时，自动切到对应选中节点上
                    if (target.sign !== this.pointer.sign) {
                        target = this.getSign(target.sign, target.popup);
                    }
                    return this.checkFocus(target);
                }
                else {
                    return this.checkPosition(0, 0);
                }
            }
        };
    }
    /**
     * 移动到当前焦点所在行的最后一个|移动标记到焦点所在行的最后一个
     * @param createrOrFlag 是否限定区域|含有sign标记的焦点对象
     * @returns Creater | undefined
     */
    get end() {
        return (createrOrFlag) => {
            if (isObject(createrOrFlag)) {
                let creater = createrOrFlag;
                if (creater.sign) {
                    let next = creater;
                    let prev = creater;
                    while ((prev = this.rulesInSign('right', prev))) {
                        next = prev;
                    }
                    if (next !== creater) {
                        creater.remove(this.selectName);
                        next.add(this.selectName);
                    }
                    return next;
                }
                else {
                    console.error(`您当前选择的焦点对象，未含有sign标记`);
                }
            }
            else {
                if (this.pointer) {
                    let flag = createrOrFlag;
                    let target = this.pointer;
                    let tempObj;
                    // 仅在焦点标记的区域、或默认全局移动到左侧边界
                    while ((tempObj = this.rulesInArea('right', target, flag, this.popSign))) {
                        target = tempObj;
                    }
                    // 移动到不同标记的焦点时，自动切到对应选中节点上
                    if (target.sign !== this.pointer.sign) {
                        target = this.getSign(target.sign, target.popup);
                    }
                    return this.checkFocus(target);
                }
                else {
                    return this.checkPosition(0, 0);
                }
            }
        };
    }
    /**
     * 清除当前层级焦点内容
     */
    get clearFocus() {
        return () => {
            let popup = this.popSign;
            this.ergodicViews((viewItem) => {
                let layer = viewItem.getLayer(popup);
                if (layer) {
                    let { pointer } = layer;
                    if (pointer) {
                        pointer.remove(this.focusName);
                        pointer.blur && isFunction(pointer.blur) && pointer.blur(pointer);
                    }
                    layer.setPointer();
                }
                return false;
            });
        };
    }
    /**
     * 处理同一标记类型，当前选中状态
     * @param sign 元素标记名称
     * @param fn 返回布尔类型来标记当前焦点选中或未选中
     * @param popup 节点所在层级标记（非必传）
     */
    get checkSign() {
        return (sign, fn, popup) => {
            let tempArr = this.getSignList(sign, popup);
            tempArr.forEach((createrItem) => {
                createrItem.selected = fn(createrItem);
                // 添加移除选中类名
                createrItem.selected
                    ? createrItem.add(this.selectName)
                    : createrItem.remove(this.selectName);
            });
        };
    }
    /**
     * 根据弹层和标签标记名称获取标记列表
     * @param createrOrSign 分组标记名称
     * @param popup 弹层标记名称
     * @returns Creater[]
     */
    get getSignList() {
        return (createrOrSign, popup) => {
            let list = [];
            if (isObject(createrOrSign)) {
                let creater = createrOrSign;
                list = creater.__sign__ || [];
            }
            else if (isString(createrOrSign)) {
                let sign = createrOrSign;
                this.ergodicViews((viewItem) => {
                    let pool = viewItem.getLayer(popup);
                    if (pool !== null) {
                        let temp = pool.getSignList(sign);
                        if (temp) {
                            list = temp;
                            return true;
                        }
                    }
                    return false;
                });
            }
            return list;
        };
    }
    /**
     * 获取指定标记类型选中状态的数据内容
     * @param createrOrSign 元素标记名称|焦点元素
     * @param popup 节点所在层级标记（非必传）
     * @returns Creater | undefined
     */
    get getSign() {
        return (createrOrSign, popup) => {
            var _a;
            let data;
            if (createrOrSign instanceof Creater) {
                let creater = createrOrSign;
                (_a = creater.__sign__) === null || _a === void 0 ? void 0 : _a.forEach((createrItem) => {
                    createrItem.selected && (data = createrItem);
                });
                if (!data) {
                    creater.add(this.selectName);
                    creater.selected = true;
                    data = creater;
                }
            }
            else if (typeof createrOrSign === "string") {
                let sign = createrOrSign;
                let tempArr = this.getSignList(sign, popup);
                tempArr.forEach((createrItem) => {
                    createrItem.selected && (data = createrItem);
                });
                !data && console.warn(`您查看的标记为${createrOrSign}的列表中，未查找到已选中元素`);
            }
            return data;
        };
    }
    /**
     * 下一个标记移动方法
     * @param sign 标记名称
     * @param direction 方向（默认为向右移动）
     * @param popup 弹窗标记，非必传
     * @returns Creater | undefined
     */
    get nextSign() {
        return (sign, direction = 'right', popup) => {
            var _a, _b;
            let prev = this.getSign(sign, popup);
            let next = this.rulesInSign(direction, prev);
            if (next !== undefined) {
                prev === null || prev === void 0 ? void 0 : prev.remove(this.selectName);
                (_a = prev === null || prev === void 0 ? void 0 : prev.__pools__) === null || _a === void 0 ? void 0 : _a.setPointer();
                next.add(this.selectName);
                (_b = next.__pools__) === null || _b === void 0 ? void 0 : _b.setPointer(next);
            }
            return next;
        };
    }
    /**
     * 上一个标记移动方法
     * @param sign 标记名称
     * @param direction 方向（默认为向左移动）
     * @param popup 弹窗标记，非必传
     * @returns Creater | undefined
     */
    get prevSign() {
        return (sign, direction = 'left', popup) => {
            var _a, _b;
            let prev = this.getSign(sign, popup);
            let next = prev !== undefined ? this.rulesInSign(direction, prev) : undefined;
            if (next) {
                prev === null || prev === void 0 ? void 0 : prev.remove(this.selectName);
                (_a = prev === null || prev === void 0 ? void 0 : prev.__pools__) === null || _a === void 0 ? void 0 : _a.setPointer();
                next.add(this.selectName);
                (_b = next.__pools__) === null || _b === void 0 ? void 0 : _b.setPointer(next);
            }
            return next;
        };
    }
}

class Service extends MoveRule {
    constructor(app, options) {
        super(options);
        Object.defineProperty(this, "_monitor", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: true
        });
        Object.defineProperty(this, "_vm", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: void 0
        });
        Object.defineProperty(this, "activeListenFlag", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: false
        });
        Object.defineProperty(this, "_durationState", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: false
        });
        Object.defineProperty(this, "_durationTimeout", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: 0
        });
        Object.defineProperty(this, "_baseEvents", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: { default: { rootPopup: {} } }
        });
        Object.defineProperty(this, "popEvents", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: { rootPopup: {} }
        });
        Object.defineProperty(this, "_version", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: version
        });
        Object.defineProperty(this, "defOption", {
            enumerable: true,
            configurable: true,
            writable: true,
            value: {
                inactiveTime: 0,
                limitTime: 200
            }
        });
        if (options) {
            let { inactiveTime, limitTime, back } = options;
            inactiveTime !== undefined && (this.defOption.inactiveTime = Math.max(convertUnits(inactiveTime), 300000));
            limitTime !== undefined && (this.defOption.limitTime = Math.max(convertUnits(limitTime), 200));
            back !== undefined && (this.defOption.back = back);
        }
        this._vm = app;
        this.resetEvent();
        this.startActiveListen();
        this.startEventListen();
    }
    get monitor() {
        return this._monitor;
    }
    set monitor(flag) {
        this._monitor = flag;
        flag && this.getEventName("ENTER");
    }
    /**
     * 查看版本号
     */
    get version() {
        return this._version;
    }
    /**
     * 重置按键响应事件
     */
    get resetEvent() {
        return () => {
            for (const popEvents of Object.values(this._baseEvents)) {
                for (const prop of Object.keys(popEvents.rootPopup)) {
                    const eName = prop;
                    popEvents.rootPopup[eName] = undefined;
                }
            }
        };
    }
    /**
     * 添加事件
     * @param eventObj 事件对象
     * @param popup 弹层
     */
    get addEvent() {
        return (eventObj, popup) => {
            const viewName = this.getRouteName();
            const popupStr = getPopupStr(popup);
            !Object.keys(this._baseEvents).includes(viewName) && (this._baseEvents[viewName] = {});
            Object.keys(this._baseEvents[viewName]).includes(popupStr) ?
                Object.assign(this._baseEvents[viewName][popupStr], eventObj) :
                (this._baseEvents[viewName][popupStr] = eventObj);
        };
    }
    getRouteName() {
        let viewName = '';
        const instance = vue.getCurrentInstance();
        if (this._vm.config.globalProperties.$router && instance) {
            const { type } = instance;
            const route = vueRouter.useRoute();
            // 获取当前视图名称
            for (const record of route.matched) {
                for (const [name, component] of Object.entries(record.components)) {
                    if (component === type) {
                        viewName = name;
                        break;
                    }
                }
            }
        }
        return viewName || "default";
    }
    startEventListen() {
        let limitTime = convertUnits(this.defOption.limitTime);
        let startTime = new Date().getTime();
        let storeKey;
        const eventDown = (event) => {
            event = event || window.event;
            // 组织默认事件兼容处理
            if (typeof event.cancelable !== 'boolean' || event.cancelable) {
                event.preventDefault();
            }
            else {
                console.warn("The following event couldn't be canceled:");
            }
            // // 阻止事件冒泡
            // if (event.stopPropagation) {
            //   event.stopPropagation()
            // } else if (typeof window !== 'undefined') {
            //   event.cancelBubble = true
            // }
            let keyCode = event.which ? event.which : event.keyCode;
            if (this.useKeyUp) {
                if (!storeKey) {
                    storeKey = keyCode;
                    startTime = Date.now();
                }
            }
            else {
                let currentTime = Date.now();
                let keyAction = this.getKeyAction(keyCode);
                let difference = currentTime - startTime;
                // 使用节流的形式限制按键响应
                if (keyAction && this.monitor && difference > limitTime) {
                    startTime = currentTime;
                    let eventName = this.getEventName(keyAction);
                    eventName !== "" && this.callEvent(eventName, event);
                }
                if (this.activeListenFlag) {
                    // 配置时长超过5分钟时生效
                    this.exitInactive();
                    this.timeoutInactive();
                }
            }
        };
        const eventUp = (event) => {
            event = event || window.event;
            // 组织默认事件兼容处理
            if (typeof event.cancelable !== 'boolean' || event.cancelable) {
                event.preventDefault();
            }
            else {
                console.warn("The following event couldn't be canceled:");
            }
            // // 阻止事件冒泡
            // if (event.stopPropagation) {
            //   event.stopPropagation()
            // } else if (typeof window !== 'undefined') {
            //   event.cancelBubble = true
            // }
            let currentTime = Date.now();
            let keyCode = event.which ? event.which : event.keyCode;
            let keyAction = this.getKeyAction(keyCode);
            if (keyAction && this.monitor && keyCode === storeKey) {
                let longFlag = false;
                if (currentTime - startTime >= 650) {
                    longFlag = true;
                }
                let eventName = this.getEventName(keyAction, longFlag);
                eventName !== "" && this.callEvent(eventName, event);
            }
            storeKey = null;
            if (this.activeListenFlag) {
                // 配置时长超过5分钟时生效
                this.exitInactive();
                this.timeoutInactive();
            }
        };
        const eventOver = (event) => {
            event = event || window.event;
            let parent = event.target;
            while (parent && parent._$FocusCreater === undefined) {
                parent = parent.parentElement;
            }
            if (parent._$FocusCreater) {
                const creater = parent._$FocusCreater;
                creater.name === "items" && getPopupStr(creater.popup) === getPopupStr(this.popSign) && creater !== this.pointer && this.initFocus(creater);
                if (this.activeListenFlag) {
                    // 配置时长超过5分钟时生效
                    this.exitInactive();
                    this.timeoutInactive();
                }
            }
        };
        const eventClick = (event) => {
            event = event || window.event;
            let parent = event.target;
            while (parent._$FocusCreater === undefined) {
                parent = parent.parentElement;
            }
            if (parent._$FocusCreater) {
                const creater = parent._$FocusCreater;
                if (creater.name === "items" && getPopupStr(creater.popup) === getPopupStr(this.popSign)) {
                    this.pointer !== creater && this.initFocus(creater);
                    this.callEvent("keyEnter", event);
                }
                if (this.activeListenFlag) {
                    // 配置时长超过5分钟时生效
                    this.exitInactive();
                    this.timeoutInactive();
                }
            }
        };
        // 事件监听
        if (document.addEventListener) {
            document.addEventListener("mouseover", eventOver, true);
            document.addEventListener("click", eventClick, true);
            document.addEventListener('keydown', eventDown, true);
            // } else if ((document as any).attachEvent) {
            //   (document as any).attachEvent("onkeydown", eventDown);
            // } else {
            //   (document as any).onkeydown = eventDown
        }
        if (this.useKeyUp) {
            if (document.addEventListener) {
                document.addEventListener('keyup', eventUp, true);
                // } else if ((document as any).attachEvent) {
                //   (document as any).attachEvent("onkeyup", eventDown);
                // } else {
                //   (document as any).onkeyup = eventDown
            }
        }
    }
    callEvent(eventName, event) {
        let viewName = this.pointer ? this.pointer.routeViewName : "default";
        !Object.keys(this._baseEvents).includes(viewName) && (viewName = "default");
        const popSignStr = getPopupStr(this.popSign);
        if (Object.keys(this._baseEvents[viewName]).includes(popSignStr)) {
            if (Object.keys(this._baseEvents[viewName][popSignStr]).includes(eventName)) {
                this._baseEvents[viewName][popSignStr][eventName](event);
            }
            if (["keyLeft", "keyRight", "keyUp", "keyDown", "keyBack"].includes(eventName)) {
                switch (eventName) {
                    case "keyLeft":
                        this.moveFocus("left");
                        break;
                    case "keyRight":
                        this.moveFocus("right");
                        break;
                    case "keyUp":
                        this.moveFocus("up");
                        break;
                    case "keyDown":
                        this.moveFocus("down");
                        break;
                    case "keyBack":
                        popSignStr === "rootPopup" && (this.defOption.back !== undefined && typeof this.defOption.back === "function" ? this.defOption.back(event) : history.back());
                        break;
                }
            }
        }
    }
    /**
     * 判断并启动非活跃状态监听
     */
    startActiveListen() {
        this.activeListenFlag = false;
        if (this.defOption.inactiveTime !== undefined && this.defOption.InterruptInactive !== undefined && this.defOption.inactiveCallback !== undefined) {
            this.activeListenFlag = true;
            // 配置时长超过5分钟时生效
            this.exitInactive();
            this.timeoutInactive();
        }
    }
    /**
     * 退出非活跃状态
     */
    exitInactive() {
        var _a;
        if (this._durationState) {
            this._durationState = false;
            (_a = this.defOption.InterruptInactive) === null || _a === void 0 ? void 0 : _a.call(this);
        }
    }
    /**
     * 非活跃状态计时
     */
    timeoutInactive() {
        if (this._durationState) {
            this._durationTimeout = 0;
        }
        else {
            clearTimeout(this._durationTimeout);
            this._durationTimeout = setTimeout(() => {
                var _a;
                // 满足不活跃时长后执行对应方法
                this._durationState = true;
                (_a = this.defOption.inactiveCallback) === null || _a === void 0 ? void 0 : _a.call(this);
            }, convertUnits(this.defOption.inactiveTime));
        }
    }
    /**
     * 检测当前视图是否有未卸载的焦点/视图对象
     * @param views 当前节点所在视图对象
     * @returns 所有子视图、焦点是否已卸载
     */
    hasChildren(views) {
        let flag = false;
        for (let [propName, value] of Object.entries(views)) {
            if (propName === 'layers') {
                for (const layerPool of Object.values(value)) {
                    // 检测当前view的焦点列表是否卸载完
                    flag = flag || !!layerPool.items.length;
                    if (flag) {
                        break;
                    }
                }
            }
            else if (propName === 'routeViews') {
                for (const routeProp of Object.values(value)) {
                    // 检测当前view是否含有子view
                    flag = flag || !!Object.keys(routeProp).length;
                    if (flag) {
                        break;
                    }
                }
            }
            if (flag) {
                break;
            }
        }
        return !flag;
    }
    /**
     * 注册节点
     * @param {Creater} creater 节点对象
     */
    get register() {
        return (creater) => {
            var _a, _b;
            let { appoints, name, routeDepath, routeVpath, routerEl, sign } = creater;
            let views = this.getView(routeDepath, routeVpath);
            // 添加views指向
            creater.__views__ = views;
            views.registeLayer(creater);
            if (!views.__baseView__) {
                views.registeEl(routerEl, routeVpath, views, this);
            }
            if (name === 'items') {
                // 添加默认选中状态类名
                sign && creater.selected && creater.add(this.defOption.selectName);
                // 添加默认焦点
                if (appoints && !((_a = creater.__pools__) === null || _a === void 0 ? void 0 : _a.pointer)) {
                    (_b = creater.__pools__) === null || _b === void 0 ? void 0 : _b.setPointer(creater);
                    creater.add(this.defOption.focusName);
                }
            }
        };
    }
    /**
     * 更新焦点/区域
     * @param elem 元素
     * @param binding 绑定信息
     */
    get updater() {
        return (elem, binding) => {
            let creater = elem._$FocusCreater;
            creater === null || creater === void 0 ? void 0 : creater.update(binding);
        };
    }
    /**
     * 卸载焦点及对应view
     * @param elem 元素
     */
    get remover() {
        return (elem) => {
            var _a, _b, _c;
            let creater = elem._$FocusCreater;
            if (creater !== undefined) {
                (_a = creater.__pools__) === null || _a === void 0 ? void 0 : _a.splicePool(creater);
                // 清空所有引用
                if (!((_b = creater.__pools__) === null || _b === void 0 ? void 0 : _b.items.length)) {
                    // 解绑当前pools
                    if (creater.popup) {
                        // 节点在弹窗上，直接移除当前view上对应的弹窗属性
                        const propStr = `popup_${creater.popup}`;
                        (_c = creater.__views__) === null || _c === void 0 ? true : delete _c.layers[propStr];
                        this.popEvents[creater.popup] && delete this.popEvents[creater.popup];
                    }
                    let views = creater.__views__;
                    while (views && this.hasChildren(views)) {
                        if (views.parent) {
                            for (const [propName, value] of Object.entries(views.parent.routeViews)) {
                                for (const [viewProp, viewVal] of Object.entries(value)) {
                                    if (viewVal === views) {
                                        delete value[viewProp];
                                    }
                                }
                                if (!Object.keys(value).length) {
                                    delete views.parent.routeViews[propName];
                                    if (views.parent.viewSign === propName) {
                                        views.parent.viewSign = '';
                                    }
                                }
                            }
                        }
                        views = views.parent;
                    }
                }
                creater.__views__ = undefined;
                creater.__pools__ = undefined;
                creater.__group__ = undefined;
                elem._$FocusCreater = undefined;
            }
        };
    }
}

var keepAliveStore = pinia.defineStore('focusKeepAlive', () => {
    const includeStr = vue.ref('');
    const setPages = (pageNames) => {
        includeStr.value = pageNames;
    };
    const getPages = () => includeStr.value;
    const delPage = (pageName) => {
        includeStr.value = includeStr.value
            .split(',')
            .filter((item) => item !== pageName)
            .join(',');
    };
    return {
        includeStr,
        setPages,
        getPages,
        delPage
    };
});

var index = (app, options) => {
    if (!app.config.globalProperties.$Focus) {
        const service = new Service(app, options);
        app.config.globalProperties.$Focus = service;
        app.directive('items', createFocusDirective(service, 'items'));
        app.directive('group', createFocusDirective(service, 'group'));
        const router = app.config.globalProperties.$router;
        if (router) {
            router.beforeEach((to, from) => {
                service.monitor = false;
                if (from.name !== to.name) {
                    // 不同页面跳转时初始化按键响应内容
                    service.resetEvent();
                }
                service.createView(to);
                return true;
            });
            router.afterEach(() => {
                service.monitor = true;
                (options === null || options === void 0 ? void 0 : options.includeAttr) && keepAliveStore().setPages(options === null || options === void 0 ? void 0 : options.includeAttr);
            });
        }
        else {
            console.warn("路由配置未注册/未使用路由功能！");
        }
    }
    else {
        console.warn("焦点插件已注册，请勿重复注册！");
    }
};

exports.default = index;
exports.keepAliveStore = keepAliveStore;
//# sourceMappingURL=epg-focus-vue.cjs.js.map
