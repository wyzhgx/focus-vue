import { Directive } from 'vue';
import { FocusBinding } from './types/focus';
import Service from './utils/service';
import Creater from './types/creater';

export const createFocusDirective = (services: Service, name: 'items' | 'group'): Directive => ({
  mounted(el: HTMLElement, binding: FocusBinding) {
    services.register(new Creater(name, el, binding, services));
  },
  updated(el: HTMLElement, binding: FocusBinding) {
    services.updater(el, binding);
  },
  beforeUnmount(el: HTMLElement) {
    services.remover(el);
  }
});
