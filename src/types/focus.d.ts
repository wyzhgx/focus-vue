import { DirectiveBinding } from 'vue';
import { Router } from 'vue-router';
import Creater from './creater';

declare enum FocusModifyer {
  sign = 'sign',
  group = 'group',
  popup = 'popup',
  assign = 'assign',
  appoints = 'appoints',
  blur = 'blur',
  focus = 'focus',
  mouseover = 'mouseover',
  click = 'click'
}
type FocusArgMap = {
  [FocusModifyer.sign]: string;
  [FocusModifyer.group]: string;
  [FocusModifyer.popup]: string;
  [FocusModifyer.assign]: boolean;
  [FocusModifyer.appoints]: boolean;
  [FocusModifyer.blur]: (pointer?: Creater) => void;
  [FocusModifyer.focus]: (pointer?: Creater) => void;
  [FocusModifyer.mouseover]: (event?: Event) => void;
  [FocusModifyer.click]: (event?: Event) => void;
};

// 主类型定义
type FocusBindType<T extends FocusModifyer | undefined = undefined> = T extends FocusModifyer ? { def: any } & { [K in T]: FocusArgMap[K] } : any;

export interface FocusBinding<T extends FocusModifyer | undefined = undefined> extends DirectiveBinding<FocusBindType<T>> {
  modifiers: { [K in FocusModifyer]?: boolean };
}

/**
 * 焦点路由信息
 */
export interface FocusRouteObj {
  /**
   * 路由深度，0对应routeView外部，1对应最外层的routeView
   */
  routeDepath: number;
  /**
   * 虚拟路径，以routeView为基准，routeview外部为public，内部以routeView的name为准
   */
  routeVpath: string;
  /**
   * routeView对应的html，默认为document.documentElement
   */
  routerEl: HTMLElement;
  routeViewName: string;
}

export interface OptionType {
  selectName?: string;
  focusName?: string;
  failoverDist?: number;
  includeAttr?: string;
  limitTime?: number | string;
  inactiveTime?: string | number;
  useKeyUp?: boolean;
  keyMaps?: { [key in FocusKeyMaysProps]: number | number[] };
  inactiveCallback?: () => void;
  InterruptInactive?: () => void;
  back?: (event: Event) => void;
}
export type DirectionStr = "left" | "right" | "up" | "down"

export type FocusKeyMaysKeys =
  | 'LEFT'
  | 'UP'
  | 'RIGHT'
  | 'DOWN'
  | 'ENTER'
  | 'BACK'
  | 'NUMBER'
  | 'FOWARD'
  | 'REWIND'
  | 'PLAY'
  | 'PAUSE'
  | 'STOP'
  | 'DELETE';

export type FocusKeyMaysProps =
  | 'left'
  | 'LEFT'
  | 'up'
  | 'UP'
  | 'right'
  | 'RIGHT'
  | 'down'
  | 'DOWN'
  | 'enter'
  | 'ENTER'
  | 'back'
  | 'BACK'
  | 'number'
  | 'NUMBER'
  | 'foward'
  | 'FOWARD'
  | 'rewind'
  | 'REWIND'
  | 'play'
  | 'PLAY'
  | 'pause'
  | 'PAUSE'
  | 'stop'
  | 'STOP'
  | 'delete'
  | 'DELETE';

export type FocusEventName =
  | 'keyLeft'
  | 'keyUp'
  | 'keyRight'
  | 'keyDown'
  | 'keyBack'
  | 'keyEnter'
  | 'keyNumber'
  | 'keyFoward'
  | 'keyRewind'
  | 'keyPlay'
  | 'keyPause'
  | 'keyStop'
  | 'keyDelete'
  | 'keyLongLeft'
  | 'keyLongRight'
  | 'keyLongUp'
  | 'keyLongDown'
  | 'keyLongEnter'
  | 'keyLongBack'
  | 'keyLongNumber'
  | 'keyLongFoward'
  | 'keyLongRewind'
  | 'keyLongPlay'
  | 'keyLongPause'
  | 'keyLongStop'
  | 'keyLongDelete';

export interface EpgFocusEvent {
  [viewName: string]: { [popup: string]: { [eventName in FocusEventName]?: (event: Event) => void } }
}
