import type { ComponentInternalInstance } from 'vue';
import Creater from './creater';
import Service from '../utils/service';
import { Store, StoreDefinition } from 'pinia';

declare global {
  interface HTMLElement {
    _boundEvents?: { name: string; handler: void }[]; // 自定义属性类型
    _$FocusClass?: Set<string>;
    _$FocusCreater?: Creater;
    __vueParentComponent?: ComponentInternalInstance;
  }
}

declare module '@vue/runtime-core' {
  interface ComponentCustomProperties {
    $Focus: Service;
    keepAliveStore: Store
  }
}

export { };
