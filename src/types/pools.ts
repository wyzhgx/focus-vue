import Creater from './creater';
import { hasOwn } from '../utils/valid';

/**
 * 创建页面/弹窗，焦点信息基础列表
 */
export default class Pools {
  protected groups: { [key: string]: Creater };
  items: Creater[] = [];
  pointer?: Creater;
  protected signFlag: { [key: string]: boolean };
  protected signItems: { [key: string]: Creater[] };
  protected groupList: { [key: string]: Creater[] };
  constructor() {
    this.groups = {};
    this.signFlag = {};
    this.signItems = {};
    this.groupList = {};
  }

  /**
   * 赋值焦点
   * @param {Creater} creater 焦点对象
   */
  setPointer(creater?: Creater) {
    this.pointer = creater;
  }

  /**
   * 检测标记默认选中状态
   * @param {String} sign 节点分组标记名称
   * @returns Boolean
   */
  checkSign(sign: string): boolean {
    return this.signFlag[sign] || false;
  }

  /**
   * 获取对应标记列表
   * @param {String} sign 标记名称
   * @returns Array|null
   */
  getSignList(sign: string): Creater[] | null {
    return hasOwn(this.signItems, sign) ? this.signItems[sign] : null;
  }

  /**
   * 获取标记列表，如果没有则创建新数组
   * @param {String} sign 标记名称
   * @returns Array
   */
  checkOrCreateSignList(sign: string): Creater[] {
    return hasOwn(this.signItems, sign) ? this.signItems[sign] : (this.signItems[sign] = []);
  }

  getInnerGroupList(group: string): Creater[] {
    return hasOwn(this.groupList, group) ? this.groupList[group] : [];
  }

  /**
   * 获取标记列表，如果没有则创建新数组
   * @param {String} sign 标记名称
   * @returns Array
   */
  checkOrCreateGroupList(group: string): Creater[] {
    return hasOwn(this.groupList, group) ? this.groupList[group] : (this.groupList[group] = []);
  }

  /**
   * 注册焦点
   * @param {Creater} creater 焦点的对象
   */
  registePool(creater: Creater) {
    let { name, sign, group, selected } = creater;
    if (name === 'group') {
      !sign && console.error(`${creater.data}绑定的group区域未添加标识`);
      if (hasOwn(this.groups, sign)) {
        console.warn(`标识为${sign}的group区域已经注册过，请勿重复注册`);
      } else {
        this.groups[sign] = creater;
      }
    }
    if (group) {
      // 焦点移入对应分组列表
      const _groupList = this.checkOrCreateGroupList(group);
      _groupList.push(creater);
      creater.__group__ = this.groups[group];
      creater.__list__ = _groupList;
      this.groupList[group].push(creater);
    } else {
      // 焦点移入通用焦点列表
      this.items.push(creater);
      creater.__list__ = this.items;
    }
    if (name === 'items' && !!sign) {
      if (this.checkSign(sign)) {
        creater.selected = false;
      } else {
        this.signFlag[sign] = selected;
      }
      /**
       * 同步推入相关标记列表
       */
      let signList = this.checkOrCreateSignList(sign);
      signList.push(creater);
      creater.__sign__ = signList;
    }
  }

  /**
   * 移除焦点对象
   * @param {Creater} creater 焦点对象
   */
  splicePool(creater: Creater) {
    let { id, name, sign, group } = creater;
    if (name === 'items') {
      // 判断是否为当前pools的焦点
      this.pointer === creater && this.setPointer();
      // 判断是否含有标记，并从对应标记列表中移除
      if (sign) {
        let signIndex: number | undefined = undefined;
        creater.__sign__?.forEach((item, key) => {
          if (item.id === id) {
            signIndex = key;
          }
        });
        if (signIndex !== undefined) {
          creater.__sign__?.splice(signIndex, 1);
        }
        this.signFlag[sign] = !!creater.__sign__?.filter((item: Creater) => item.selected).length;
        if (!creater.__sign__?.length) {
          // 所有该标记的节点都卸载之后，移除该标记
          delete this.signFlag[sign];
          delete this.signItems[sign];
        }
      }
    } else if (name === 'group') {
      delete this.groups[sign];
    }
    let listIndex: number | undefined = undefined;
    creater.__list__?.forEach((item, key) => {
      if (item.id === id) {
        listIndex = key;
      }
    });
    if (listIndex !== undefined) {
      creater.__list__?.splice(listIndex, 1);
    }
    if (!creater.__list__?.length && group) {
      delete this.groupList[sign];
    }
  }
}
