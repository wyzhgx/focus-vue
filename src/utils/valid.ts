const toString = Object.prototype.toString;

/**
 * 判断是否为对象类型
 * @param obj 任意数据
 * @returns boolean
 */
export const isObject = (obj: any): boolean => toString.call(obj) === '[object Object]';

/**
 * 判断是否为字符串类型
 * @param str 任意数据
 * @returns boolean
 */
export const isString = (str: any): boolean => toString.call(str) === '[object String]';

/**
 * 判断是否为数组类型
 * @param arr 任意数据
 * @returns boolean
 */
export const isArray = (arr: any): boolean => toString.call(arr) === '[object Array]';

/**
 * 判断是否为函数类型
 * @param fun 任意数据
 * @returns boolean
 */
export const isFunction = (fun: any): boolean => toString.call(fun) === '[object Function]';

/**
 * 判断是否为数字类型
 * @param num 任意数据
 * @returns boolean
 */
export const isNumber = (num: any): boolean => toString.call(num) === '[object Number]';

/**
 * 判断是否为布尔类型
 * @param flag 任意数据
 * @returns boolean
 */
export const isBoolean = (flag: any): boolean => toString.call(flag) === '[object Boolean]';

/**
 * 判断是否为undefined
 * @param undef 任意数据
 * @returns boolean
 */
export const isUndefined = (undef: any): boolean => toString.call(undef) === '[object Undefined]';

/**
 * 判断是否为null
 * @param nul 任意数据
 * @returns boolean
 */
export const isNull = (nul: any): boolean => toString.call(nul) === '[object Null]';

/**
 * 判断对象是否含有对应的字段
 * @param obj 对象
 * @param prop 字段名称
 * @returns boolean
 */
export const hasOwn = (obj: object, prop: string): boolean => Object.prototype.hasOwnProperty.call(obj, prop);
