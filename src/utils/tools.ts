import { ComponentInternalInstance } from 'vue';
import { FocusRouteObj } from '../types/focus';

/**
 * 设置元素属性
 * @param el 元素节点
 * @param name 名称
 * @param value 值
 */
export const setDataSet = (el: HTMLElement, name: string, value?: string) => {
  let reg = new RegExp('([A-Z])', 'g');
  name = 'data-' + name.replace(reg, (match: string, letter: string) => '-' + letter.toLowerCase());
  el.setAttribute(name, value || '');
};

/**
 * 获取节点路由区域信息
 * @param elem 元素节点
 * @returns 焦点路由相关信息
 */
export const checkRouteViewDepath = (elem: HTMLElement): FocusRouteObj => {
  let currentNode: ComponentInternalInstance | null | undefined = elem.__vueParentComponent;
  let routeDepath: number = 0;
  let routeVpath: string = '';
  let routeViewName: string = "default";
  let routerEl: HTMLElement = document.documentElement;
  let list: string[] = [];
  while (currentNode) {
    if (currentNode.type.name === 'RouterView') {
      if (!list.length) {
        routerEl = currentNode.vnode.el as HTMLElement;
        routeViewName = currentNode.props.name as string;
      }
      list.unshift(parseStr(currentNode.props.name));
      routeDepath++;
    }
    currentNode = currentNode?.parent as ComponentInternalInstance;
  }
  list.unshift('public');
  routeVpath = list.join(':::');
  return { routeDepath, routeVpath, routerEl, routeViewName };
};

/**
 * 计算元素节点位置信息
 * @param el DOM节点
 * @param key 方向
 * @returns number
 */
export const getOffsetParent = (el: HTMLElement, key: 'top' | 'left') => {
  let offset: number = 0;
  let parentNode: HTMLElement = el;
  var offsetFStr: 'offsetTop' | 'offsetLeft' = ('offset' + key.replace(/^([a-z])(.+)/, (match, a, b) => a.toUpperCase() + b)) as
    | 'offsetTop'
    | 'offsetLeft';
  do {
    offset += parentNode[offsetFStr];
    if (parentNode.offsetParent instanceof HTMLElement) {
      parentNode = parentNode.offsetParent;
    } else {
      break;
    }
  } while (parentNode);
  return offset;
};

/**
 * 将undefined、null等转化为""
 * @param str 字符串
 * @returns string
 */
export const parseStr = (str: any): string => {
  if (!str || str === 'undefined' || str === 'null') {
    return '';
  }
  return str;
};

/**
 * 时间单位转换，单位毫秒
 * @param time 时间
 * @returns number
 */
export const convertUnits = (time: string | number): number => {
  if (typeof time === "number") {
    return time;
  } else if (typeof time === "string") {
    let tempStr = time;
    if (/^\d*[hH]$/.test(tempStr)) {
      return parseInt(tempStr.replace(/[hH]/, '')) * 60 * 60 * 1000;
    } else if (/^\d*[mM]$/.test(tempStr)) {
      return parseInt(tempStr.replace(/[mM]/, '')) * 60 * 1000;
    } else if (/^\d*[sS]$/.test(tempStr)) {
      return parseInt(tempStr.replace(/[sS]/, '')) * 1000;
    } else if (/^\d*$/.test(tempStr)) {
      return parseInt(tempStr);
    }
  }
  console.warn('时长设置无效或未知单位!');
  return 0;
};

/**
 * 获取弹窗标记
 * @param popup 弹窗标记
 * @returns 弹窗名称
 */
export const getPopupStr = (popup?: string) => {
  return popup ? popup === "rootPopup" ? "rootPopup" : 'popup_' + popup : 'rootPopup';
}

export default {
  getPopupStr,
  setDataSet,
  checkRouteViewDepath,
  getOffsetParent,
  parseStr
};
