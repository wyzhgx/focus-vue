import { defineStore } from 'pinia';
import { ref } from 'vue';
export default defineStore('focusKeepAlive', () => {
  const includeStr = ref<string>('');
  const setPages = (pageNames: string) => {
    includeStr.value = pageNames;
  };
  const getPages = () => includeStr.value;
  const delPage = (pageName: string) => {
    includeStr.value = includeStr.value
      .split(',')
      .filter((item) => item !== pageName)
      .join(',');
  };
  return {
    includeStr,
    setPages,
    getPages,
    delPage
  };
});
